﻿using NodeResources;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace SatelliteData
{
    public class ConnectionManager : IConnectionManager
    {
        private string _serverName { get; set; }
        private string _databaseName { get; set; }
        private string _user { get; set; }
        private string _password { get; set; }
        public string ConnString { get; set; }

        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }
        public string DatabaseName
        {
            get { return _databaseName; }
            set { _databaseName = value; }
        }

        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public ConnectionManager()
        {
            ConnectionStringSettings _settings = ConfigurationManager.ConnectionStrings["SatelliteEntity"];
            ConnString = _settings.ConnectionString.ToString();
        }

        public ConnectionManager(string connectionString)
        {
            if (connectionString != null)
            {
                var connElements = connectionString.Split(';');

                foreach (var v in connElements)
                {
                    var s = v.Split('=');
                    switch (s[0].ToLower())
                    {
                        case "data source":
                            ServerName = s[1];
                            break;
                        case "initial catalog":
                            DatabaseName = s[1];
                            break;
                        case "user id":
                            User = s[1];
                            break;
                        case "password":
                            Password = s[1];
                            break;

                    }
                }

            }

        }

        public ConnectionManager(string server, string database, string userName, string password)
        {
            Password = password;
            DatabaseName = database;
            ServerName = server;
            User = userName;
            ConnString = BuildConnectionString(ServerName, DatabaseName, User, Password);
        }

        public string BuildConnectionString(string server, string database, string user, string password)
        {
            Password = password;
            DatabaseName = database;
            ServerName = server;
            User = user;
            var sqlBuilder = new SqlConnectionStringBuilder
            {
                DataSource = ServerName,
                InitialCatalog = DatabaseName,
                UserID = User,
                Password = Password
            };
            return sqlBuilder.ToString();

        }

        public string BuildEntityConnectionString(string sqlString)
        {
            EntityConnectionStringBuilder esb = new EntityConnectionStringBuilder();
            //esb.Metadata = "res://*/Models.NodeModel.csdl|res://*/Models.NodeModel.ssdl|res://*/Models.NodeModel.msl";
            esb.Provider = "System.Data.SqlClient";
            esb.ProviderConnectionString = sqlString;
            return esb.ToString();

        }
    }
}