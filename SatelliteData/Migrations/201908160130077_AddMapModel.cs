namespace SatelliteData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddMapModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MapOperations",
                c => new
                {
                    MD_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    MD_MapDescription = c.String(nullable: false, maxLength: 100),
                    MD_Type = c.String(nullable: false, maxLength: 4),
                    MD_FromField = c.String(maxLength: 50),
                    MD_ToField = c.String(maxLength: 50),
                    MD_DataType = c.String(),
                })
                .PrimaryKey(t => t.MD_ID);

        }

        public override void Down()
        {
            DropTable("dbo.MapOperations");
        }
    }
}
