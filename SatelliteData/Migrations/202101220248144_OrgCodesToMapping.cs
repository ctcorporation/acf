﻿namespace SatelliteData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrgCodesToMapping : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MapOperations", "MD_FromOrgCode", c => c.String(maxLength: 15));
            AddColumn("dbo.MapOperations", "MD_ToOrgCode", c => c.String(maxLength: 15));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MapOperations", "MD_ToOrgCode");
            DropColumn("dbo.MapOperations", "MD_FromOrgCode");
        }
    }
}
