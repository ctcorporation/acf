﻿namespace SatelliteData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class transactionfileNamelengthFinal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "T_FILENAME", c => c.String(maxLength: 255, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "T_FILENAME", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
