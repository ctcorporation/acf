namespace SatelliteData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cargowise_Enums",
                c => new
                    {
                        CW_ID = c.Guid(nullable: false),
                        CW_ENUMTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
                        CW_ENUM = c.String(maxLength: 10, fixedLength: true),
                        CW_MAPVALUE = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.CW_ID);
            
            CreateTable(
                "dbo.CargowiseContext",
                c => new
                    {
                        CC_ID = c.Guid(nullable: false),
                        CC_Context = c.String(maxLength: 50, unicode: false),
                        CC_Description = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.CC_ID);
            
            CreateTable(
                "dbo.Contact",
                c => new
                    {
                        S_ID = c.Guid(nullable: false),
                        S_CONTACTNAME = c.String(maxLength: 50, unicode: false),
                        S_EMAILADDRESS = c.String(unicode: false),
                        S_C = c.Guid(),
                        S_ALERTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.S_ID)
                .ForeignKey("dbo.Customer", t => t.S_C)
                .Index(t => t.S_C);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        C_ID = c.Guid(nullable: false),
                        C_NAME = c.String(maxLength: 50, unicode: false),
                        C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        C_PATH = c.String(maxLength: 80, unicode: false),
                        C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        C_CODE = c.String(maxLength: 15, unicode: false),
                        C_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        C_TRIALSTART = c.DateTime(),
                        C_TRIALEND = c.DateTime(),
                        C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.C_ID);
            
            CreateTable(
                "dbo.DTS",
                c => new
                    {
                        D_ID = c.Guid(nullable: false),
                        D_C = c.Guid(),
                        D_INDEX = c.Int(),
                        D_FINALPROCESSING = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        D_P = c.Guid(),
                        D_FILETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        D_DTSTYPE = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        D_DTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        D_SEARCHPATTERN = c.String(unicode: false),
                        D_NEWVALUE = c.String(unicode: false),
                        D_QUALIFIER = c.String(unicode: false),
                        D_TARGET = c.String(unicode: false),
                        D_CURRENTVALUE = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.D_ID);
            
            CreateTable(
                "dbo.Mapping_Definition",
                c => new
                    {
                        M_ID = c.Guid(nullable: false),
                        M_DataFrom = c.String(maxLength: 50),
                        M_DataTo = c.String(maxLength: 50),
                        M_FieldFrom = c.String(maxLength: 50),
                        M_FieldTo = c.String(maxLength: 50),
                        M_DataType = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        M_Length = c.Int(),
                        M_P = c.Guid(),
                    })
                .PrimaryKey(t => t.M_ID);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        PW_ID = c.Guid(nullable: false),
                        PW_ARTICLEID = c.String(maxLength: 10, fixedLength: true),
                        PW_PARTNUM = c.String(maxLength: 50, unicode: false),
                        PW_PRODUCTNAME = c.String(maxLength: 150, unicode: false),
                        PW_BARCODE = c.String(maxLength: 20, unicode: false),
                        PW_UOM = c.String(maxLength: 50, unicode: false),
                        PW_CWUOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        PW_IMPORTED = c.DateTime(),
                        PW_LASTUPDATE = c.DateTime(),
                        PW_CARGOWISEREPLY = c.DateTime(),
                        PW_CHILDARTICLE = c.String(maxLength: 10, fixedLength: true),
                        PW_CHILDQTY = c.Int(),
                        PW_CHILDUOM = c.String(maxLength: 3, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.PW_ID);
            
            CreateTable(
                "dbo.PROFILE",
                c => new
                    {
                        P_ID = c.Guid(nullable: false),
                        P_C = c.Guid(),
                        P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        P_SERVER = c.String(unicode: false),
                        P_USERNAME = c.String(unicode: false),
                        P_PASSWORD = c.String(unicode: false),
                        P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
                        P_DESCRIPTION = c.String(unicode: false),
                        P_PATH = c.String(maxLength: 100, unicode: false),
                        P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        P_LIBNAME = c.String(maxLength: 50, unicode: false),
                        P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
                        P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
                        P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
                        P_SENDERID = c.String(maxLength: 15, unicode: false),
                        P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
                        P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
                        P_SUBJECT = c.String(maxLength: 100, unicode: false),
                        P_FILETYPE = c.String(maxLength: 7, fixedLength: true, unicode: false),
                        P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
                        P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
                        P_GROUPCHARGES = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        P_XSD = c.String(maxLength: 50, unicode: false),
                        P_PARAMLIST = c.String(unicode: false),
                        P_METHOD = c.String(maxLength: 50, unicode: false),
                        P_NOTIFY = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        P_NOTIFYEMAIL = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.P_ID);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        T_ID = c.Guid(nullable: false),
                        T_C = c.Guid(),
                        T_P = c.Guid(),
                        T_DATETIME = c.DateTime(),
                        T_FILENAME = c.String(maxLength: 100, unicode: false),
                        T_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        T_INVOICED = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        T_INVOICEDATE = c.DateTime(),
                        T_MSGTYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                        T_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
                        T_DIRECTION = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        T_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        T_REF1 = c.String(maxLength: 50),
                        T_REF2 = c.String(maxLength: 50),
                        T_REF3 = c.String(maxLength: 50),
                        T_ARCHIVE = c.String(maxLength: 100),
                        T_REF1TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                        T_REF2TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                        T_REF3TYPE = c.String(maxLength: 10, fixedLength: true, unicode: false),
                    })
                .PrimaryKey(t => t.T_ID)
                .ForeignKey("dbo.PROFILE", t => t.T_P)
                .Index(t => t.T_P);
            
            CreateTable(
                "dbo.Transaction_Log",
                c => new
                    {
                        X_ID = c.Guid(nullable: false),
                        X_P = c.Guid(),
                        X_FILENAME = c.String(maxLength: 150, unicode: false),
                        X_DATE = c.DateTime(),
                        X_SUCCESS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
                        X_C = c.Guid(),
                        X_LASTRESULT = c.String(maxLength: 120, unicode: false),
                    })
                .PrimaryKey(t => t.X_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "T_P", "dbo.PROFILE");
            DropForeignKey("dbo.Contact", "S_C", "dbo.Customer");
            DropIndex("dbo.Transactions", new[] { "T_P" });
            DropIndex("dbo.Contact", new[] { "S_C" });
            DropTable("dbo.Transaction_Log");
            DropTable("dbo.Transactions");
            DropTable("dbo.PROFILE");
            DropTable("dbo.Product");
            DropTable("dbo.Mapping_Definition");
            DropTable("dbo.DTS");
            DropTable("dbo.Customer");
            DropTable("dbo.Contact");
            DropTable("dbo.CargowiseContext");
            DropTable("dbo.Cargowise_Enums");
        }
    }
}
