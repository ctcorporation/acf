namespace SatelliteData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogsToTransaction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "T_LOGMSG", c => c.String());
            AddColumn("dbo.Transactions", "T_LOGTYPE", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "T_LOGTYPE");
            DropColumn("dbo.Transactions", "T_LOGMSG");
        }
    }
}
