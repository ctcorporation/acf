namespace SatelliteData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRequiredUnusedFieldsFromTransaction : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Transactions", "T_TRIAL");
            DropColumn("dbo.Transactions", "T_INVOICED");
            DropColumn("dbo.Transactions", "T_INVOICEDATE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transactions", "T_INVOICEDATE", c => c.DateTime());
            AddColumn("dbo.Transactions", "T_INVOICED", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
            AddColumn("dbo.Transactions", "T_TRIAL", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
        }
    }
}
