namespace SatelliteData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class FileDescriptionAndHeartBeat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileDescriptions",
                c => new
                {
                    PC_Id = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    PC_P = c.Guid(),
                    PC_FileName = c.String(),
                    PC_HasHeader = c.Boolean(nullable: false),
                    PC_Delimiter = c.String(maxLength: 1),
                    PC_FieldCount = c.Int(nullable: false),
                    PC_FirstFieldName = c.String(),
                    PC_LastFieldName = c.String(),
                    PC_Quotations = c.Boolean(nullable: false),
                    PC_HeaderStart = c.Int(nullable: false),
                    PC_DataStart = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.PC_Id)
                .ForeignKey("dbo.PROFILE", t => t.PC_P)
                .Index(t => t.PC_P);

        }

        public override void Down()
        {
            DropForeignKey("dbo.FileDescriptions", "PC_P", "dbo.PROFILE");
            DropIndex("dbo.FileDescriptions", new[] { "PC_P" });
            DropTable("dbo.FileDescriptions");
        }
    }
}
