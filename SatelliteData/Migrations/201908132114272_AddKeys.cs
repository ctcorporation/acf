namespace SatelliteData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddKeys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contact", "S_C", "dbo.Customer");
            DropForeignKey("dbo.Transactions", "T_P", "dbo.PROFILE");
            DropPrimaryKey("dbo.Cargowise_Enums");
            DropPrimaryKey("dbo.CargowiseContext");
            DropPrimaryKey("dbo.Contact");
            DropPrimaryKey("dbo.Customer");
            DropPrimaryKey("dbo.DTS");
            DropPrimaryKey("dbo.Mapping_Definition");
            DropPrimaryKey("dbo.Product");
            DropPrimaryKey("dbo.PROFILE");
            DropPrimaryKey("dbo.Transactions");
            DropPrimaryKey("dbo.Transaction_Log");
            AddColumn("dbo.Customer", "testfield", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Cargowise_Enums", "CW_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.CargowiseContext", "CC_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.Contact", "S_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.Customer", "C_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.DTS", "D_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.Mapping_Definition", "M_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.Product", "PW_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.PROFILE", "P_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.Transactions", "T_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.Transaction_Log", "X_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AddPrimaryKey("dbo.Cargowise_Enums", "CW_ID");
            AddPrimaryKey("dbo.CargowiseContext", "CC_ID");
            AddPrimaryKey("dbo.Contact", "S_ID");
            AddPrimaryKey("dbo.Customer", "C_ID");
            AddPrimaryKey("dbo.DTS", "D_ID");
            AddPrimaryKey("dbo.Mapping_Definition", "M_ID");
            AddPrimaryKey("dbo.Product", "PW_ID");
            AddPrimaryKey("dbo.PROFILE", "P_ID");
            AddPrimaryKey("dbo.Transactions", "T_ID");
            AddPrimaryKey("dbo.Transaction_Log", "X_ID");
            AddForeignKey("dbo.Contact", "S_C", "dbo.Customer", "C_ID");
            AddForeignKey("dbo.Transactions", "T_P", "dbo.PROFILE", "P_ID");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "T_P", "dbo.PROFILE");
            DropForeignKey("dbo.Contact", "S_C", "dbo.Customer");
            DropPrimaryKey("dbo.Transaction_Log");
            DropPrimaryKey("dbo.Transactions");
            DropPrimaryKey("dbo.PROFILE");
            DropPrimaryKey("dbo.Product");
            DropPrimaryKey("dbo.Mapping_Definition");
            DropPrimaryKey("dbo.DTS");
            DropPrimaryKey("dbo.Customer");
            DropPrimaryKey("dbo.Contact");
            DropPrimaryKey("dbo.CargowiseContext");
            DropPrimaryKey("dbo.Cargowise_Enums");
            AlterColumn("dbo.Transaction_Log", "X_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.Transactions", "T_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.PROFILE", "P_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.Product", "PW_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.Mapping_Definition", "M_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.DTS", "D_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.Customer", "C_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.Contact", "S_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.CargowiseContext", "CC_ID", c => c.Guid(nullable: false));
            AlterColumn("dbo.Cargowise_Enums", "CW_ID", c => c.Guid(nullable: false));
            DropColumn("dbo.Customer", "testfield");
            AddPrimaryKey("dbo.Transaction_Log", "X_ID");
            AddPrimaryKey("dbo.Transactions", "T_ID");
            AddPrimaryKey("dbo.PROFILE", "P_ID");
            AddPrimaryKey("dbo.Product", "PW_ID");
            AddPrimaryKey("dbo.Mapping_Definition", "M_ID");
            AddPrimaryKey("dbo.DTS", "D_ID");
            AddPrimaryKey("dbo.Customer", "C_ID");
            AddPrimaryKey("dbo.Contact", "S_ID");
            AddPrimaryKey("dbo.CargowiseContext", "CC_ID");
            AddPrimaryKey("dbo.Cargowise_Enums", "CW_ID");
            AddForeignKey("dbo.Transactions", "T_P", "dbo.PROFILE", "P_ID");
            AddForeignKey("dbo.Contact", "S_C", "dbo.Customer", "C_ID");
        }
    }
}
