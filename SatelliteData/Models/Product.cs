namespace SatelliteData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Product")]
    public partial class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PW_ID { get; set; }

        [StringLength(10)]
        public string PW_ARTICLEID { get; set; }

        [StringLength(50)]
        public string PW_PARTNUM { get; set; }

        [StringLength(150)]
        public string PW_PRODUCTNAME { get; set; }

        [StringLength(20)]
        public string PW_BARCODE { get; set; }

        [StringLength(50)]
        public string PW_UOM { get; set; }

        [StringLength(3)]
        public string PW_CWUOM { get; set; }

        public DateTime? PW_IMPORTED { get; set; }

        public DateTime? PW_LASTUPDATE { get; set; }

        public DateTime? PW_CARGOWISEREPLY { get; set; }

        [StringLength(10)]
        public string PW_CHILDARTICLE { get; set; }

        public int? PW_CHILDQTY { get; set; }

        [StringLength(3)]
        public string PW_CHILDUOM { get; set; }
    }
}
