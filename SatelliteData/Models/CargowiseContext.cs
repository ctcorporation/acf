namespace SatelliteData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CargowiseContext")]
    public partial class CargowiseContext
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CC_ID { get; set; }

        [StringLength(50)]
        public string CC_Context { get; set; }

        public string CC_Description { get; set; }
    }
}
