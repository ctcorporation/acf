﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SatelliteData.Models
{
    public class MapOperation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MD_ID { get; set; }

        [MaxLength(100)]
        [Required]
        public string MD_MapDescription { get; set; }

        [MaxLength(4)]
        [Required]
        public string MD_Type { get; set; }

        [MaxLength(50)]
        public string MD_FromField { get; set; }

        [MaxLength(50)]
        public string MD_ToField { get; set; }

        public string MD_DataType { get; set; }
        [MaxLength(15)]
        public string MD_FromOrgCode { get; set; }
        [MaxLength(15)]
        public string MD_ToOrgCode { get; set; }


    }
}
