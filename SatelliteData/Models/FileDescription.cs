﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SatelliteData.Models
{
    public class FileDescription
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PC_Id { get; set; }

        [ForeignKey("LookupProfile")]
        public Guid? PC_P { get; set; }

        public virtual Profile LookupProfile { get; set; }

        public string PC_FileName { get; set; }

        [DefaultValue(false)]
        public bool PC_HasHeader { get; set; }

        [MaxLength(1)]
        public string PC_Delimiter { get; set; }

        public int PC_FieldCount { get; set; }

        public string PC_FirstFieldName { get; set; }

        public string PC_LastFieldName { get; set; }

        [DefaultValue(false)]
        public bool PC_Quotations { get; set; }

        public int PC_HeaderStart { get; set; }

        public int PC_DataStart { get; set; }
    }
}
