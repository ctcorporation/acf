namespace SatelliteData.Models
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.SqlServer;

    //public class NodeDBConfiguration : DbConfiguration
    //{

    //    public NodeDBConfiguration()
    //    {
    //        SetTransactionHandler(SqlProviderServices.ProviderInvariantName, () => new CommitFailureHandler());
    //        SetExecutionStrategy(SqlProviderServices.ProviderInvariantName, () => new SqlAzureExecutionStrategy());
    //    }


    //}
    public partial class CNodeEntity : DbContext
    {
        public CNodeEntity()
            : base("name=CNodeEntity")
        {
        }

        public virtual DbSet<HEARTBEAT> HEARTBEATs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HEARTBEAT>()
                .Property(e => e.HB_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<HEARTBEAT>()
                .Property(e => e.HB_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<HEARTBEAT>()
                .Property(e => e.HB_ISMONITORED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HEARTBEAT>()
                .Property(e => e.HB_LASTOPERATION)
                .IsUnicode(false);
        }
    }
}
