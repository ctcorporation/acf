namespace SatelliteData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Cargowise_Enums
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CW_ID { get; set; }

        [StringLength(20)]
        public string CW_ENUMTYPE { get; set; }

        [StringLength(10)]
        public string CW_ENUM { get; set; }

        [StringLength(20)]
        public string CW_MAPVALUE { get; set; }
    }
}
