namespace SatelliteData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Transaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid T_ID { get; set; }

        public Guid? T_C { get; set; }

        [ForeignKey("TransactionProfile")]
        public Guid? T_P { get; set; }

        public virtual Profile TransactionProfile { get; set; }

        public DateTime? T_DATETIME { get; set; }

        [StringLength(255)]
        public string T_FILENAME { get; set; }


        [StringLength(10)]
        public string T_MSGTYPE { get; set; }

        [StringLength(15)]
        public string T_BILLTO { get; set; }

        [StringLength(1)]
        public string T_DIRECTION { get; set; }

        [StringLength(1)]
        public string T_CHARGEABLE { get; set; }

        [StringLength(50)]
        public string T_REF1 { get; set; }

        [StringLength(50)]
        public string T_REF2 { get; set; }

        [StringLength(50)]
        public string T_REF3 { get; set; }

        [StringLength(100)]
        public string T_ARCHIVE { get; set; }

        [StringLength(10)]
        public string T_REF1TYPE { get; set; }

        [StringLength(10)]
        public string T_REF2TYPE { get; set; }

        [StringLength(10)]
        public string T_REF3TYPE { get; set; }

        public string T_LOGMSG { get; set; }

        [StringLength(10)]
        public string T_LOGTYPE { get; set; }
    }
}
