namespace SatelliteData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Contact")]
    public partial class Contact
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid S_ID { get; set; }

        [StringLength(50)]
        public string S_CONTACTNAME { get; set; }

        public string S_EMAILADDRESS { get; set; }

        [ForeignKey("ContactCustomer")]
        public Guid? S_C { get; set; }

        public virtual Customer ContactCustomer { get; set; }

        [StringLength(1)]
        public string S_ALERTS { get; set; }
    }
}
