namespace SatelliteData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Mapping_Definition
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid M_ID { get; set; }

        [StringLength(50)]
        public string M_DataFrom { get; set; }

        [StringLength(50)]
        public string M_DataTo { get; set; }

        [StringLength(50)]
        public string M_FieldFrom { get; set; }

        [StringLength(50)]
        public string M_FieldTo { get; set; }

        [StringLength(3)]
        public string M_DataType { get; set; }

        public int? M_Length { get; set; }

        public Guid? M_P { get; set; }
    }
}
