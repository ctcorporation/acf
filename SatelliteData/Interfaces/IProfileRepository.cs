﻿using SatelliteData.Models;

namespace SatelliteData.Interfaces
{
    public interface IProfileRepository : IRepository<Profile>
    {
    }
}
