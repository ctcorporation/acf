﻿using SatelliteData.Models;

namespace SatelliteData.Interfaces
{
    public interface ITransactionRepository : IRepository<Transaction>
    {

    }
}
