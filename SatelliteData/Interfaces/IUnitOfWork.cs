﻿using System;

namespace SatelliteData.Interfaces
{
    interface IUnitOfWork : IDisposable
    {
        ITransactionRepository Transactions
        {
            get;
        }

        int Complete();
    }

}

