﻿using SatelliteData.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;


namespace SatelliteData
{
    public static class HeartBeat
    {
        private static List<Assembly> GetListOfEntryAssemblyWithReferences()
        {
            List<Assembly> listOfAssemblies = new List<Assembly>();
            var mainAsm = Assembly.GetEntryAssembly();
            listOfAssemblies.Add(mainAsm);

            foreach (var refAsmName in mainAsm.GetReferencedAssemblies())
            {
                listOfAssemblies.Add(Assembly.Load(refAsmName));
            }
            return listOfAssemblies;
        }
        public static void RegisterHeartBeat(string custCode, Guid custID, string operation, ParameterInfo[] parameters, string pathParam)
        {
            Process currentProcess = Process.GetCurrentProcess();
            var appParam = Assembly.GetEntryAssembly().GetName().Name;
            Process proc = Process.GetCurrentProcess();
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);

            DateTime? checkinParam = null;
            DateTime? openParam = null;
            int pid = currentProcess.Id;
            if (operation == "Starting")
            {
                checkinParam = DateTime.Now;
                openParam = DateTime.Now;

            }
            if (operation == "Stopping")
            {
                pid = 0;
                openParam = null;
                checkinParam = null;


            }
            string pList = string.Empty;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    pList += parameters[i].Name + ": " + parameters[i].ToString();
                }
            }
            try
            {
                using (CNodeEntity db = new CNodeEntity())
                {
                    var hb = (from h in db.HEARTBEATs
                              where h.HB_NAME == appParam
                              select h).FirstOrDefault();
                    if (hb == null)
                    {

                        HEARTBEAT newHeartBeart = new HEARTBEAT
                        {
                            HB_C = custID,
                            HB_PATH = pathParam,
                            HB_PID = pid,
                            HB_LASTOPERATIONPARAMS = pList,
                            HB_LASTCHECKIN = checkinParam,
                            HB_LASTOPERATION = operation,
                            HB_NAME = appParam,
                            HB_OPENED = openParam,
                            HB_ISMONITORED = "Y"
                        };
                        db.HEARTBEATs.Add(newHeartBeart);
                    }
                    else
                    {
                        if (openParam != null)
                        {
                            hb.HB_OPENED = openParam;
                        }
                        hb.HB_LASTCHECKIN = DateTime.Now;
                        hb.HB_LASTOPERATION = operation;
                        hb.HB_LASTOPERATIONPARAMS = pList;
                        hb.HB_PID = pid;
                        hb.HB_PATH = pathParam;
                    }
                    db.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }

        }
    }
}
