﻿using SatelliteData.Interfaces;
using SatelliteData.Models;

namespace SatelliteData.Repository
{
    public class ProfileRepository : Repository<Profile>, IProfileRepository
    {
        public ProfileRepository(SatelliteEntity context)
            : base(context)
        {

        }

        public SatelliteEntity SatelliteEntity
        {
            get
            {
                return Context as SatelliteEntity;
            }
        }
    }
}
