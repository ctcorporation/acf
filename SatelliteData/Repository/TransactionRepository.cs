﻿using SatelliteData.Interfaces;
using SatelliteData.Models;

namespace SatelliteData.Repository
{
    class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(SatelliteEntity context)
            : base(context)
        {

        }

        public SatelliteEntity SatelliteEntity
        {
            get { return Context as SatelliteEntity; }

        }
    }


}
