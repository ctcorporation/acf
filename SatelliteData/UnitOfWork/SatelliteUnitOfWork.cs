﻿using SatelliteData.Interfaces;
using SatelliteData.Models;
using SatelliteData.Repository;
using System;
using System.Data.Entity.Validation;

namespace SatelliteData.UnitOfWork
{
    public class SatelliteUnitOfWork : ISatelliteUnitOfWork
    {
        private readonly SatelliteEntity _context;
        private bool disposed = false;
        private string _errorLog;
        public SatelliteUnitOfWork(SatelliteEntity context)
        {
            _context = context;
            Profiles = new ProfileRepository(_context);
            Transactions = new TransactionRepository(_context);

        }

        public IProfileRepository Profiles
        {
            get;
            private set;
        }

        public ITransactionRepository Transactions
        {
            get;
            private set;
        }
        public string ErrorLog { get { return _errorLog; } set { _errorLog = value; } }

        public int Complete()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                _errorLog += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
            }
            catch (DbEntityValidationException ex)
            {
                //var errorMessages = ex.EntityValidationErrors
                //    .SelectMany(x => x.ValidationErrors)
                //    .Select(x => x.ErrorMessage);

                //// Join the list to a single string.
                //var fullErrorMessage = string.Join("; ", errorMessages);

                //// Combine the original exception message with the new one.
                //var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                //_errorLog += exceptionMessage + Environment.NewLine;
                ////Throw a new DbEntityValidationException with the improved exception message.
                //throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                // List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        _errorLog += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                    }
                }
            }
            return -1;
        }



        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
