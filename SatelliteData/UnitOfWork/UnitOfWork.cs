﻿using SatelliteData.Interfaces;
using SatelliteData.Models;
using SatelliteData.Repository;
using System;
using System.Data.Entity.Validation;
using System.Linq;

namespace SatelliteData.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private string _errorLog;
        public string ErrorLog
        {
            get
            { return _errorLog; }
            set
            {
                _errorLog = value;
            }
        }
        private readonly SatelliteEntity _context;

        private bool disposed = false;
        public UnitOfWork(SatelliteEntity context)
        {
            _context = context;
            Transactions = new TransactionRepository(_context);
        }

        public int Complete()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                _errorLog += exceptionMessage + Environment.NewLine;
                //Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                //List<string> errorMessages = new List<string>();
                //foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                //{
                //    string entityName = validationResult.Entry.Entity.GetType().Name;
                //    foreach (DbValidationError error in validationResult.ValidationErrors)
                //    {
                //        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                //    }
                //}
            }
            return -1;
        }

        public ITransactionRepository Transactions
        {
            get;
            private set;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            if (disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

        }
    }
}
