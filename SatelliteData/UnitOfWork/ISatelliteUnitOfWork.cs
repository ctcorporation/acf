﻿using SatelliteData.Interfaces;
using System;

namespace SatelliteData.UnitOfWork
{
    public interface ISatelliteUnitOfWork : IDisposable
    {
        IProfileRepository Profiles { get; }

        ITransactionRepository Transactions { get; }

        int Complete();
        string ErrorLog { get; set; }
    }

}