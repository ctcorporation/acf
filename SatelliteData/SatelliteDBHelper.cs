﻿using SatelliteData.Models;
using System.Linq;

namespace SatelliteData
{
    public class SatelliteDBHelper
    {
        private string _connString { get; set; }

        public SatelliteDBHelper(string connString)
        {
            _connString = connString;
        }

        public bool ValidProduct(Product product)
        {

            using (SatelliteEntity db = new SatelliteEntity(_connString))
            {
                var prod = (from p in db.Products
                            where p.PW_PARTNUM == product.PW_PARTNUM
                            select p).FirstOrDefault();
                if (prod != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public string GetBarcodeFromProductCode(string code)
        {
            using (SatelliteEntity db = new SatelliteEntity(_connString))
            {
                var prod = (from p in db.Products
                            where p.PW_PARTNUM == code
                            select p.PW_BARCODE).FirstOrDefault();
                return prod;


            }
        }
    }
}
