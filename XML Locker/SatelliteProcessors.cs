﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using XML_Locker.CTC;

namespace XMLLocker
{
    public class SatelliteProcessors
    {
        private string _connString { get; set; }

        public SatelliteProcessors(string connString)
        {
            _connString = connString;

        }

        public NodeFile AITToCommon(Profile profile, string xmlfile)
        {
            NodeFile nodeFile = new NodeFile();
            FileInfo processingFile = new FileInfo(xmlfile);
            XDocument aitFile = XDocument.Load(xmlfile);

            var companyList = GetCompanyListFromOrderFile(aitFile);

            NodeFileOperation nodeFileOperation = new NodeFileOperation();
            nodeFileOperation.TrackingOrders = new List<NodeFileOperationTrackingOrder> { _order }.ToArray();
            NodeFileIdendtityMatrix identityMatrix = new NodeFileIdendtityMatrix();
            identityMatrix.CustomerId = profile.C_CODE;
            identityMatrix.DocumentIdentifier = _order.OrderNo;
            identityMatrix.SenderId = profile.P_SENDERID;
            identityMatrix.DocumentType = "TrackingOrder";
            identityMatrix.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            identityMatrix.OriginalFileName = fileName;
            NodeFile ctcCommon = new NodeFile
            {

                Operation = nodeFileOperation,
                IdendtityMatrix = identityMatrix

            };
            return ctcCommon;
        }

        private List<CompanyElement> GetCompanyListFromOrderFile(XDocument aitFile)
        {
            var CompanyListing = aitFile.Descendants("SalesOrder").Elements("Companies");
            List<CompanyElement> _companies = new List<CompanyElement>();
            foreach (var a in CompanyListing)
            {

            }

            CompanyElement supplier = new CompanyElement();
            return _companies;
        }
    }
}
