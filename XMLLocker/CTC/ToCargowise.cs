﻿using CTCLogging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using XMLLocker.CW;

namespace XMLLocker.CTC
{
    public class ToCargowise
    {
        private string _appLogPath { get; set; }
        public ToCargowise(string logPath)
        {
            _appLogPath = logPath;
        }


        public bool CreateProductFile(NodeFile common, XMLLocker.Cargowise.NDM.Action partOperation, string outputPath)
        {
            if (common.Operation.ProductImport != null)
            {
                string lo = this._appLogPath;
                string cp = string.Empty;
                string de = "Processing Folder Queue";
                string fu = "Process Pickup";
                DateTime ti = DateTime.Now;
                string er = string.Empty;
                string di = string.Empty;
                foreach (ProductElement product in common.Operation.ProductImport)
                {
                    int iRec = 0;
                    try
                    {
                        Cargowise.NDM.UniversalInterchange universalInterchange = new Cargowise.NDM.UniversalInterchange();
                        Cargowise.NDM.UniversalInterchangeHeader universalInterchangeHeader = new Cargowise.NDM.UniversalInterchangeHeader();
                        Cargowise.NDM.UniversalInterchangeBody universalInterchangeBody = new Cargowise.NDM.UniversalInterchangeBody();
                        Cargowise.NDM.ProductData productData = new Cargowise.NDM.ProductData();
                        Cargowise.NDM.NativeProduct nativeProduct = new Cargowise.NDM.NativeProduct();
                        List<Cargowise.NDM.NativeProductOrgPartRelation> nativeProductOrgRelationColl = new List<Cargowise.NDM.NativeProductOrgPartRelation>();
                        Cargowise.NDM.NativeProductOrgPartRelation nativeProductOrgPartRelation = new Cargowise.NDM.NativeProductOrgPartRelation();
                        Cargowise.NDM.NativeProductOrgPartRelationOrgHeader orgPartRelationOrgHeader = new Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                        List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode> nativeProductBarcodeColl = new List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                        Cargowise.NDM.NativeProductOrgSupplierPartBarcode nativeProductBarcode = new Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                        List<Cargowise.NDM.NativeProductOrgPartUnit> nativeProductOrgPartUnitColl = new List<Cargowise.NDM.NativeProductOrgPartUnit>();
                        iRec++;

                        nativeProduct = new Cargowise.NDM.NativeProduct();
                        nativeProduct.PartNum = product.Code;
                        nativeProduct.StockKeepingUnit = product.StockKeepingUnit;
                        nativeProduct.Weight = product.Weight > 0 ? product.Weight : 0;
                        nativeProduct.WeightSpecified = product.Weight > 0 ? true : false;
                        nativeProduct.WeightUQ = product.WeightUnit != null ? product.WeightUnit : null;
                        nativeProduct.WeightSpecified = product.WeightUnit != null ? true : false;
                        nativeProduct.Height = product.Height > 0 ? product.Height : 0;
                        nativeProduct.HeightSpecified = product.Height > 0 ? true : false;
                        nativeProduct.Width = product.Width > 0 ? product.Width : 0;
                        nativeProduct.WidthSpecified = product.Width > 0 ? true : false;
                        nativeProduct.Depth = product.Depth > 0 ? product.Depth : 0;
                        nativeProduct.DepthSpecified = product.Depth > 0 ? true : false;
                        int volFactor = 1;
                        switch (product.DimsUnit)
                        {
                            case "CM":
                                volFactor = 100;
                                break;
                            case "IN":
                                volFactor = 1;
                                break;
                            case "M":
                                volFactor = 1;
                                break;

                            default:
                                volFactor = 100;
                                break;
                        }
                        decimal volume = 0;
                        if (nativeProduct.Height > 0 && nativeProduct.Width > 0 && nativeProduct.Depth > 0)
                        {
                            volume = ((product.Height / volFactor) * (product.Width / volFactor) * (product.Depth / volFactor));
                        }
                        nativeProduct.Cubic = volume > 0 ? volume : 0;
                        nativeProduct.CubicSpecified = volume > 0 ? true : false;
                        nativeProduct.CubicUQ = volume > 0 ? "M3" : null;
                        nativeProduct.MeasureUQ = product.DimsUnit != null ? product.DimsUnit : null;
                        nativeProduct.Desc = product.Description;
                        nativeProduct.ActionSpecified = true;
                        nativeProduct.Action = partOperation;
                        nativeProduct.IsActive = true;
                        nativeProduct.IsWarehouseProduct = true;
                        nativeProduct.IsWarehouseProductSpecified = true;
                        nativeProduct.IsBarcoded = true;
                        nativeProduct.IsBarcodedSpecified = true;
                        orgPartRelationOrgHeader = new Cargowise.NDM.NativeProductOrgPartRelationOrgHeader();
                        nativeProductOrgRelationColl = new List<Cargowise.NDM.NativeProductOrgPartRelation>();
                        orgPartRelationOrgHeader.Code = common.IdendtityMatrix.SenderId;
                        nativeProductOrgPartRelation.OrgHeader = orgPartRelationOrgHeader;
                        nativeProductOrgPartRelation.Relationship = "OWN";
                        nativeProductOrgPartRelation.Action = partOperation;
                        nativeProductOrgPartRelation.ActionSpecified = true;
                        nativeProductOrgRelationColl.Add(nativeProductOrgPartRelation);

                        nativeProduct.OrgPartRelationCollection = nativeProductOrgRelationColl.ToArray();
                        if (product.Barcode != null)
                        {
                            nativeProductBarcodeColl = new List<Cargowise.NDM.NativeProductOrgSupplierPartBarcode>();
                            nativeProduct.IsBarcoded = true;
                            nativeProductBarcode = new Cargowise.NDM.NativeProductOrgSupplierPartBarcode();
                            nativeProductBarcode.Action = Cargowise.NDM.Action.MERGE;
                            nativeProductBarcode.ActionSpecified = true;
                            nativeProductBarcode.Barcode = product.Barcode;
                            //nativeProductBarcode.PackType.TableName = "RefPackType";
                            Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType productPackType = new Cargowise.NDM.NativeProductOrgSupplierPartBarcodePackType();
                            productPackType.TableName = "RefPackType";
                            productPackType.Code = "UNT";
                            nativeProductBarcode.PackType = productPackType;
                            nativeProductBarcodeColl.Add(nativeProductBarcode);
                            nativeProduct.OrgSupplierPartBarcodeCollection = nativeProductBarcodeColl.ToArray();
                        }
                        else
                        {
                            nativeProduct.IsBarcoded = false;
                        }
                        var cwNSUniversal = new XmlSerializerNamespaces();
                        cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                        var cwNSNative = new XmlSerializerNamespaces();
                        cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
                        productData.OrgSupplierPart = nativeProduct;
                        productData.version = "2.0";
                        nativeProduct.Action = partOperation;
                        Cargowise.NDM.Native native = new Cargowise.NDM.Native
                        {
                            Body = new Cargowise.NDM.NativeBody
                            {
                                Any = new[] { productData.AsXmlElement(cwNSNative) }
                            },
                            Header = new Cargowise.NDM.NativeHeader
                            {
                                OwnerCode = common.IdendtityMatrix.SenderId,
                                EnableCodeMapping = true,
                                EnableCodeMappingSpecified = true,
                                DataContext = new Cargowise.NDM.DataContext
                                {
                                    ActionPurpose = new Cargowise.NDM.CodeDescriptionPair { Code = "IMP", Description = "Product Import" },
                                    EventType = new Cargowise.NDM.CodeDescriptionPair { Code = "ADD", Description = "Record Added" },
                                },
                            },
                            version = "2.0"
                        };
                        universalInterchange.version = "1.1";
                        universalInterchangeHeader.SenderID = common.IdendtityMatrix.SenderId;
                        universalInterchangeHeader.RecipientID = common.IdendtityMatrix.CustomerId;
                        universalInterchangeBody.Any = new[] { native.AsXmlElement(cwNSUniversal) };
                        universalInterchange.Body = universalInterchangeBody;
                        universalInterchange.Header = universalInterchangeHeader;
                        String cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code + partOperation.ToString() + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(outputPath, common.IdendtityMatrix.SenderId + "PRODUCT" + "-" + product.Code + partOperation + iFileCount + ".xml");
                        }
                        using (Stream outputCW = File.Open(cwXML, FileMode.Create))
                        {
                            StringWriter writer = new StringWriter();
                            XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.NDM.UniversalInterchange));
                            xSer.Serialize(outputCW, universalInterchange, cwNSUniversal);
                            outputCW.Flush();
                            outputCW.Close();
                        }
                    }


                    catch (Exception ex)
                    {
                        cp = "Error Creating Product File";
                        de = "Error Processing " + product.Code;
                        ti = DateTime.Now;
                        er = "Error:" + ex.GetType().Name + " :" + ex.Message;
                        di = outputPath;
                        using (AppLogger log = new AppLogger(lo, cp, de, fu, ti, er, di))
                        {
                            log.AddLog();
                        }
                        return false;

                    }
                }
            }
            return true;
        }
        public UniversalInterchange CWFromCommon(NodeFile nodeFile)
        {
            UniversalInterchange cw = new UniversalInterchange();
            if (nodeFile.Operation != null)
            {
                if (nodeFile.Operation.TrackingOrders != null)
                {
                    if (nodeFile.Operation.TrackingOrders.Length > 0)
                    {
                        cw = CreateFramework("OrderManagerOrder");
                        cw = CreateTrackingOrder(nodeFile, CreateFramework("OrderManagerOrder"));
                    }
                }
                if (nodeFile.Operation.WarehouseOrders != null)
                {
                    if (nodeFile.Operation.WarehouseOrders.Length > 0)
                    {
                        cw = CreateFramework("WarehouseOrder");
                        cw = CreateWarehouseOrder(nodeFile, CreateFramework("WarehouseOrder"));
                    }
                }
            }
            if (cw != null)
            {
                cw.Header.SenderID = nodeFile.IdendtityMatrix.SenderId;
                cw.Header.RecipientID = nodeFile.IdendtityMatrix.CustomerId;
                return cw;
            }
            return null;
        }

        private UniversalInterchange CreateWarehouseOrder(NodeFile nodeFile, UniversalInterchange emptyXUS)
        {
            UniversalInterchange cw = new UniversalInterchange();
            cw = emptyXUS;
            Shipment shipment = cw.Body.BodyField.Shipment;
            foreach (NodeFileOperationWarehouseOrder ord in nodeFile.Operation.WarehouseOrders)
            {
                shipment.DateCollection = GetDatesFromCommon(ord.Dates);
                if (shipment.DateCollection == null)
                {
                    return null;
                }
                shipment.OrganizationAddressCollection = GetCompaniesFromCommon(ord.Companies);
                if (shipment.OrganizationAddressCollection == null)
                {
                    return null;
                }
                ShipmentOrder xmlOrder = new ShipmentOrder();
                xmlOrder.OrderNumber = ord.OrderReference;
                if (ord.CustomerReferences != null)
                {
                    var clientRef = (from r in ord.CustomerReferences
                                     where (r.RefType == "Customer Ref")
                                     select r.RefValue).FirstOrDefault();
                    if (!string.IsNullOrEmpty(clientRef))
                    {
                        xmlOrder.ClientReference = clientRef;
                    }
                }
                xmlOrder.FulfillmentRule = new CodeDescriptionPair
                {
                    Code = "NON",
                    Description = "None"
                };
                CodeDescriptionPair pickRule = new CodeDescriptionPair();

                xmlOrder.PickOption = new CodeDescriptionPair
                {
                    Code = "MAN",
                    Description = "Manual Pick"
                };
                xmlOrder.Type = new CodeDescriptionPair
                {
                    Code = "ORD",
                    Description = "ORDER"
                };
                xmlOrder.Warehouse = new ShipmentOrderWarehouse
                {
                    Code = ord.WarehouseCode
                };
                CodeDescriptionPair orderStatus = new CodeDescriptionPair();
                List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
                ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
                orderLine.Content = CollectionContent.Complete;
                xmlOrder.OrderLineCollection = GetOrderLinesFromCommon(ord.OrderLines);
                if (xmlOrder.OrderLineCollection == null)
                {
                    return null;
                }
                ShipmentNoteCollection notecollection = new ShipmentNoteCollection
                {
                    Content = CollectionContent.Partial,
                    ContentSpecified = true
                };
                shipment.Order = xmlOrder;
                notecollection.Note = GetNotesFromCommon(ord.SpecialInstructions).ToArray();
                if (notecollection.Note == null)
                {
                    return null;
                }
                notecollection.ContentSpecified = true;
                shipment.NoteCollection = notecollection;

            }
            cw.Body.BodyField.Shipment = shipment;
            cw.Header.SenderID = nodeFile.IdendtityMatrix.SenderId;
            cw.Header.RecipientID = nodeFile.IdendtityMatrix.CustomerId;
            return cw;
        }

        private void LogError(string profile, string details, string process, DateTime time, string error, string path)
        {
            var appLogger = typeof(AppLogger);
            var strArgs = new Type[] { typeof(string) };
            ConstructorInfo constructorAppLogger = appLogger.GetConstructor(strArgs);
            var objLog = constructorAppLogger.Invoke(new object[] { this._appLogPath, profile, details, process, time, error, path });
            var logMethod = appLogger.GetMethod("AddLog");
            var addLog = logMethod.Invoke(objLog, new object[] { });
        }

        private List<Note> GetNotesFromCommon(string specialInstructions)
        {
            List<Note> orderNotes = new List<Note>();
            Note orderNote = new Note();
            orderNote.Visibility = new CodeDescriptionPair
            {
                Code = "PUB",
                Description = "CLIENT-VISIBLE"
            };
            orderNote.Description = "Import Delivery Instructions";
            orderNote.IsCustomDescription = false;
            if (orderNote.NoteText != null)
            {
                orderNote.NoteText = specialInstructions;
                NoteNoteContext noteContext = new NoteNoteContext();
                noteContext.Code = "WAA";
                noteContext.Description = "Module: W - Warehouse, Direction: A - All, Freight: A - All";
                orderNote.NoteContext = noteContext;
                orderNotes.Add(orderNote);
            }

            return orderNotes;
        }

        private UniversalInterchange CreateTrackingOrder(NodeFile nodefile, UniversalInterchange emptyXUS)
        {
            UniversalInterchange cw = new UniversalInterchange();
            cw = emptyXUS;
            Shipment shipment = cw.Body.BodyField.Shipment;
            foreach (NodeFileOperationTrackingOrder ord in nodefile.Operation.TrackingOrders)
            {
                if (ord.IncoTerms != null)
                { shipment.ShipmentIncoTerm = new IncoTerm { Code = ord.IncoTerms }; }
                shipment.DateCollection = GetDatesFromCommon(ord.Dates);
                shipment.OrganizationAddressCollection = GetCompaniesFromCommon(ord.Companies);
                ShipmentOrder order = new ShipmentOrder();
                order.OrderNumber = ord.OrderNo;
                order.Status = new CodeDescriptionPair { Code = ord.OrderStatus };
                order.OrderLineCollection = GetOrderLinesFromCommon(ord.OrderLines);
                shipment.Order = order;
            }
            cw.Body.BodyField.Shipment = shipment;
            cw.Header.SenderID = nodefile.IdendtityMatrix.SenderId;
            cw.Header.RecipientID = nodefile.IdendtityMatrix.CustomerId;
            return cw;
        }

        private ShipmentOrderOrderLineCollection GetOrderLinesFromCommon(OrderLineElement[] orderLines)
        {
            ShipmentOrderOrderLineCollection linesColl = new ShipmentOrderOrderLineCollection();
            linesColl.Content = CollectionContent.Complete;
            List<ShipmentOrderOrderLineCollectionOrderLine> linesOrderLines = new List<ShipmentOrderOrderLineCollectionOrderLine>();
            try
            {
                foreach (OrderLineElement line in orderLines)
                {

                    ShipmentOrderOrderLineCollectionOrderLine oline = new ShipmentOrderOrderLineCollectionOrderLine
                    {
                        LineNumber = int.Parse(line.LineNo),
                        LineNumberSpecified = true,
                        Product = new Product
                        {
                            Code = line.Product.Code,
                            Description = line.Product.Description
                        },
                        OrderedQty = line.OrderQty,
                        PackageQty = line.PackageQty,
                        PackageQtySpecified = true,
                        OrderedQtySpecified = true
                    };
                    oline.PackageQtyUnit = new PackageType
                    {
                        Code = line.PackageUnit
                    };
                    linesOrderLines.Add(oline);
                }
                linesColl.OrderLine = linesOrderLines.ToArray();
                return linesColl;
            }
            catch (Exception)
            {

                return null;
            }

        }

        private OrganizationAddress[] GetCompaniesFromCommon(CompanyElement[] companies)
        {
            List<OrganizationAddress> addColl = new List<OrganizationAddress>();
            foreach (CompanyElement company in companies)
            {
                OrganizationAddress org = new OrganizationAddress
                {
                    CompanyName = company.CompanyName,
                    Address1 = company.Address1,
                    Address2 = company.Address2,
                    City = company.City,
                    State = company.State,
                    Postcode = company.PostCode,
                    Phone = company.PhoneNo,
                    OrganizationCode = string.IsNullOrEmpty(company.CompanyOrgCode) ? company.CompanyCode : company.CompanyOrgCode,
                    AddressOverride = true,
                    AddressOverrideSpecified = true
                };
                org.Country = new Country
                {
                    Code = company.Country == null ? null : company.Country,
                    Name = company.CountryCode == null ? null : company.CountryCode
                };
                switch (company.CompanyType)
                {
                    case CompanyElementCompanyType.Consignee:
                        org.AddressType = "ConsigneeDocumentaryAddress";
                        break;
                    case CompanyElementCompanyType.Consignor:
                        org.AddressType = "ConsignorDocumentaryAddress";
                        break;
                    case CompanyElementCompanyType.Supplier:
                        org.AddressType = "ConsignorDocumentaryAddress";
                        break;
                    case CompanyElementCompanyType.DeliveryAddress:
                        org.AddressType = "ConsigneePickupDeliveryAddress";
                        break;
                    case CompanyElementCompanyType.PickupAddress:
                        org.AddressType = "ConsignofPickupDeliveryAddress";
                        break;
                    case CompanyElementCompanyType.ForwardingAgent:
                        org.AddressType = "SendingForwarderAddress";
                        break;
                    case CompanyElementCompanyType.ImportBroker:
                        org.AddressType = "ImportBroker";
                        break;
                    case CompanyElementCompanyType.ShippingLine:
                        org.AddressType = "ShippingLineAddress";
                        break;
                    case CompanyElementCompanyType.BillToAddress:
                        org.AddressType = "SendersLocalClient";
                        break;
                    case CompanyElementCompanyType.OrderDeliveryAddress:
                        org.AddressType = "ConsigneeAddress";
                        break;
                }
                addColl.Add(org);
            }

            return addColl.ToArray();
        }

        private Date[] GetDatesFromCommon(DateElement[] commonDates)
        {
            List<Date> cwDates = new List<Date>();
            foreach (DateElement date in commonDates)
            {
                Date cwDate = new Date();
                switch (date.DateType)
                {
                    case DateElementDateType.Arrival:
                        cwDate.Type = DateType.Arrival;
                        break;
                    case DateElementDateType.Booked:
                        cwDate.Type = DateType.BookingConfirmed;
                        break;
                    case DateElementDateType.Closing:
                        cwDate.Type = DateType.CutOffDate;
                        break;
                    case DateElementDateType.CustomsCleared:
                        cwDate.Type = DateType.EntryDate;
                        break;
                    case DateElementDateType.DeliverBy:
                        cwDate.Type = DateType.ClientRequestedETA;
                        break;
                    case DateElementDateType.Delivered:
                        cwDate.Type = DateType.Delivery;
                        break;
                    case DateElementDateType.Departure:
                        cwDate.Type = DateType.Departure;
                        break;
                    case DateElementDateType.ExFactory:
                        cwDate.Type = DateType.AvailableExFactory;
                        break;
                    case DateElementDateType.InStoreDateReq:
                        cwDate.Type = DateType.DeliveryRequiredBy;
                        break;
                    case DateElementDateType.Loading:
                        cwDate.Type = DateType.LoadingDate;
                        break;
                    case DateElementDateType.Raised:
                        cwDate.Type = DateType.OrderDate;
                        break;
                    case DateElementDateType.Unloading:
                        cwDate.Type = DateType.Unpack;
                        break;
                }
                if (!string.IsNullOrEmpty(date.ActualDate))
                {
                    cwDate.Value = DateTime.Parse(date.ActualDate).ToString("yyyy-MM-ddThh:mm:ss");
                    cwDate.IsEstimate = false;
                    cwDate.IsEstimateSpecified = true;
                    cwDates.Add(cwDate);
                }
                if (!string.IsNullOrEmpty(date.EstimateDate))
                {
                    cwDate.Value = DateTime.Parse(date.ActualDate).ToString("yyyy-MM-ddThh:mm:ss");
                    cwDate.IsEstimate = true;
                    cwDate.IsEstimateSpecified = true;
                    cwDates.Add(cwDate);
                }
            }
            return cwDates.ToArray();
        }

        private UniversalInterchange CreateFramework(string dtType)
        {
            UniversalInterchange frame = new UniversalInterchange();
            Shipment shipment = new Shipment();
            DataTarget dt = new DataTarget
            {
                Type = dtType
            };
            List<DataTarget> dtColl = new List<DataTarget> { dt };
            DataContext dc = new DataContext
            {
                DataTargetCollection = dtColl.ToArray()
            };
            dc.ActionPurpose = new CodeDescriptionPair
            {
                Code = "CTC",
                Description = "Imported by CTC Node"
            };

            dc.EventType = new CodeDescriptionPair
            {
                Code = "ADD",
                Description = "Record Added"
            };
            shipment.DataContext = dc;
            UniversalInterchangeBody body = new UniversalInterchangeBody();
            UniversalShipmentData bodyData = new UniversalShipmentData();
            bodyData.version = "1.1";
            bodyData.Shipment = shipment;
            body.BodyField = bodyData;
            frame.Body = body;
            frame.Header = new UniversalInterchangeHeader();
            return frame;
        }
    }
}
