﻿using CTCLogging;
using SatelliteData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace XMLLocker.CTC
{
    public static class NodeFunctions
    {
        public static List<NodeFileOperationTrackingOrder> AddtoOrderList(List<NodeFileOperationTrackingOrder> o, List<NodeFileOperationTrackingOrder> orders)
        {
            foreach (NodeFileOperationTrackingOrder order in orders)
            {
                o.Add(order);
            }
            return o;
        }

        public static NodeFile ImportProducts(DataTable dt, string senderid, string recipientid, List<MapOperation> mapping, string logPath)
        {
            string lo = logPath;
            string cp = string.Empty;
            string fu = "CreateCWProductFile";
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            string conversionResult = string.Empty;
            NodeFile nodeFile = new NodeFile();
            NodeFileIdendtityMatrix id = new NodeFileIdendtityMatrix
            {
                CustomerId = recipientid,
                SenderId = senderid,
                DocumentType = "ProductImport"
            };
            nodeFile.IdendtityMatrix = id;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    int iRec = 0;
                    List<ProductElement> products = new List<ProductElement>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        ProductElement product = new ProductElement();
                        iRec++;
                        foreach (var map in mapping)
                        {
                            PropertyInfo prop = product.GetType().GetProperty(map.MD_FromField.Trim(), BindingFlags.Public | BindingFlags.Instance);
                            if (null != prop && prop.CanWrite)
                            {
                                switch (map.MD_DataType)
                                {
                                    case "BOO":
                                        bool valBool;
                                        if (bool.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valBool))
                                        {
                                            prop.SetValue(product, valBool, null);
                                        }
                                        break;
                                    case "DAT":
                                        DateTime valDate = new DateTime();
                                        if (DateTime.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valDate))
                                        {
                                            prop.SetValue(product, valDate, null);
                                        }
                                        break;
                                    case "NUM":
                                        Decimal valDec = 0;
                                        if (Decimal.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valDec))
                                        {
                                            prop.SetValue(product, valDec, null);
                                        }
                                        break;
                                    case "INT":
                                        int valInt = 0;
                                        if (int.TryParse(dr[map.MD_ToField.ToString().Trim()].ToString(), out valInt))
                                        {
                                            prop.SetValue(product, valInt, null);
                                        }
                                        break;
                                    case "STR":
                                        prop.SetValue(product, dr[map.MD_ToField.ToString().Trim()].ToString(), null);
                                        break;
                                }

                            }

                        }
                        
                        products.Add(product);
                    }
                    nodeFile.Operation = new NodeFileOperation
                    {
                        ProductImport = products.ToArray()
                    };

                }
                catch (Exception)
                {
                    using (AppLogger log = new AppLogger(lo, cp, conversionResult, fu, ti, string.Empty, di))
                    {
                        log.AddLog();
                    }
                    return null;
                }
                return nodeFile;


            }
            else
            {
                return null;
            }


        }
    }
}
