﻿using SatelliteData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using XML_Locker.CTC;

namespace XMLLocker
{
    public class SatelliteProcessors
    {
        private string _connString { get; set; }

        public SatelliteProcessors(string connString)
        {
            _connString = connString;

        }

        public NodeFile AITToCommon(Profile profile, string xmlfile)
        {
            NodeFile nodeFile = new NodeFile();
            FileInfo processingFile = new FileInfo(xmlfile);
            XDocument aitFile = XDocument.Load(xmlfile);

            var companyList = GetCompanyListFromOrderFile(aitFile);

            NodeFileOperation nodeFileOperation = new NodeFileOperation();
            NodeFileOperationWarehouseOrder _order = new NodeFileOperationWarehouseOrder();
            _order.Companies = companyList.ToArray();
            _order.SpecialInstructions = XMLHelper.GetNodeValue(aitFile, "SalesOrder", "SpecialInstructions");
            _order.OrderReference = XMLHelper.GetNodeValue(aitFile, "SalesOrder", "OrderNo");
            _order.OrderReference = "AIT";
            _order.Dates = GetDatesFromAITOrder(aitFile).ToArray();
            _order.OrderLines = GetOrderLinesFromAITOrder(aitFile).ToArray();

            nodeFileOperation.WarehouseOrders = new List<NodeFileOperationWarehouseOrder> { _order }.ToArray();
            NodeFileIdendtityMatrix identityMatrix = new NodeFileIdendtityMatrix();
            identityMatrix.CustomerId = profile.P_RECIPIENTID;
            identityMatrix.DocumentIdentifier = _order.OrderReference;
            identityMatrix.SenderId = profile.P_SENDERID;
            identityMatrix.DocumentType = "TrackingOrder";
            identityMatrix.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            identityMatrix.OriginalFileName = xmlfile;
            NodeFile ctcCommon = new NodeFile
            {

                Operation = nodeFileOperation,
                IdendtityMatrix = identityMatrix

            };
            return ctcCommon;
        }

        private List<OrderLineElement> GetOrderLinesFromAITOrder(XDocument aitFile)
        {
            List<OrderLineElement> result = new List<OrderLineElement>();
            var ordLines = aitFile.Descendants("SalesOrder").Elements("OrderLines");
            foreach (var l in ordLines)
            {
                OrderLineElement line = new OrderLineElement
                {
                    LineNo = l.Element("LineNo") != null ? l.Element("LineNo").Value : string.Empty,
                    UnitOfMeasure = l.Element("UnitOfMeasure") != null ? l.Element("UnitOfMeasure").Value : string.Empty,
                    OrderQty = l.Element("OrderQty") != null ? Convert.ToDecimal(l.Element("OrderQty").Value) : 0,
                    OrderQtySpecified = true,
                    UnitPrice = l.Element("UnitPrice") != null ? Convert.ToDecimal(l.Element("UnitPrice").Value) : 0,
                    UnitPriceSpecified = true,
                    UnitDiscount = l.Element("UnitDiscount") != null ? Convert.ToDecimal(l.Element("LineNo").Value) : 0,
                    ExtendedPrice = l.Element("ExtendedPrice") != null ? Convert.ToDecimal(l.Element("ExtendedPrice").Value) : 0,
                };
                ProductElement product = new ProductElement
                {
                    Code = l.Element("Product").Element("Code") != null ? l.Element("Product").Element("Code").Value : string.Empty,
                    Barcode = l.Element("Product").Element("Barcode") != null ? l.Element("Product").Element("Barcode").Value : string.Empty,
                    Description = l.Element("Product").Element("Description") != null ? l.Element("Product").Element("Description").Value : string.Empty
                };
                line.Product = product;
                result.Add(line);
            }
            return result;
        }

        private List<DateElement> GetDatesFromAITOrder(XDocument aitFile)
        {
            throw new NotImplementedException();
        }

        private List<CompanyElement> GetCompanyListFromOrderFile(XDocument aitFile)
        {
            var CompanyListing = aitFile.Descendants("SalesOrder").Elements("Companies");
            List<CompanyElement> _companies = new List<CompanyElement>();
            foreach (var a in CompanyListing)
            {

            }

            CompanyElement supplier = new CompanyElement();
            return _companies;
        }
    }
}
