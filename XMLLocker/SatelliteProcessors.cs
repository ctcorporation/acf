﻿using CTCLogging;
using NodeResources;
using SatelliteData;
using SatelliteData.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml.Linq;
using XMLLocker.CTC;
using XMLLocker.CW;

namespace XMLLocker
{
    public class SatelliteProcessors
    {

        private string _connString { get; set; }
        private string _appLogPath { get; set; }

        private string _notify { get; set; }
        private IMailServerSettings _mailServerSettings { get; set; }
        public SatelliteProcessors(string connString, string logPath, string notify, IMailServerSettings mserver)
        {
            _connString = connString;
            _appLogPath = logPath;
            _notify = notify;
            _mailServerSettings = mserver;

        }

        private void LogError(string profile, string details, string process, DateTime time, string error, string path)
        {
            var appLogger = typeof(AppLogger);
            var strArgs = new Type[] { typeof(string), typeof(string), typeof(string), typeof(string), typeof(DateTime), typeof(string), typeof(string) };
            ConstructorInfo constructorAppLogger = appLogger.GetConstructor(strArgs);
            var objLog = constructorAppLogger.Invoke(new object[] { this._appLogPath, profile, details, process, time, error, path });
            var logMethod = appLogger.GetMethod("AddLog");
            var addLog = logMethod.Invoke(objLog, new object[] { });
        }

        public UniversalInterchange AITToCommon(Profile profile, string xmlfile)
        {
            var m = MethodBase.GetCurrentMethod().Name;
            NodeFile nodeFile = new NodeFile();
            FileInfo processingFile = new FileInfo(xmlfile);
            XDocument aitFile = XDocument.Load(xmlfile);

            var companyList = GetCompanyListFromOrderFile(aitFile);

            NodeFileOperation nodeFileOperation = new NodeFileOperation();
            NodeFileOperationWarehouseOrder _order = new NodeFileOperationWarehouseOrder();
            _order.Companies = companyList.ToArray();
            if (!string.IsNullOrEmpty(XMLHelper.GetNodeValue(aitFile, "SalesOrder", "SpecialInstructions")))
            {
                _order.SpecialInstructions = XMLHelper.GetNodeValue(aitFile, "SalesOrder", "SpecialInstructions");
            }


            _order.WarehouseCode = "AIT";
            _order.OrderReference = XMLHelper.GetNodeValue(aitFile, "SalesOrder", "OrderNo");
            if (!string.IsNullOrEmpty(XMLHelper.GetNodeValue(aitFile, "CustomerReference", "RefType")))
            {
                List<CustomerReferenceElement> custRefs = new List<CustomerReferenceElement>();
                CustomerReferenceElement custRef = new CustomerReferenceElement
                {
                    RefType = "Customer Ref",
                    RefValue = XMLHelper.GetNodeValue(aitFile, "CustomerReference", "RefValue")
                };
                custRefs.Add(custRef);
                _order.CustomerReferences = custRefs.ToArray();
            };
            _order.Dates = GetDatesFromAITOrder(aitFile).ToArray();
            var orderLines = GetOrderLinesFromAITOrder(aitFile);


            if (orderLines == null)
            {
                LogError(profile.P_DESCRIPTION, "Order: " + _order.OrderReference + "Error Converting Order Lines", m, DateTime.Now, "Unable to properly import check Log file", "");
                return null;
            }
            else
            {
                _order.OrderLines = orderLines.ToArray();
            }
            nodeFileOperation.WarehouseOrders = new List<NodeFileOperationWarehouseOrder> { _order }.ToArray();
            NodeFileIdendtityMatrix identityMatrix = new NodeFileIdendtityMatrix();
            identityMatrix.CustomerId = profile.P_RECIPIENTID;
            identityMatrix.DocumentIdentifier = _order.OrderReference;
            identityMatrix.SenderId = profile.P_SENDERID;
            identityMatrix.DocumentType = "TrackingOrder";
            identityMatrix.FileDateTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm");
            identityMatrix.OriginalFileName = xmlfile;
            NodeFile ctcCommon = new NodeFile
            {

                Operation = nodeFileOperation,
                IdendtityMatrix = identityMatrix

            };
            Type type = typeof(ToCargowise);
            ConstructorInfo satConstructor = type.GetConstructor(new Type[] { typeof(string) });
            object satObject = satConstructor.Invoke(new object[] { this._appLogPath });
            var method = type.GetMethod("CWFromCommon");
            var returnObject = method.Invoke(satObject, new object[] { ctcCommon });
            if (returnObject != null)
            {
                if (returnObject.GetType() == typeof(UniversalInterchange))
                {
                    return (UniversalInterchange)returnObject;
                }
            }
            return null;

        }

        private List<OrderLineElement> GetOrderLinesFromAITOrder(XDocument aitFile)
        {
            List<OrderLineElement> _orderLines = new List<OrderLineElement>();
            var ordLines = aitFile.Descendants("SalesOrder").Elements("OrderLines").Elements();
            bool validOrder = true;
            var ordNo = XMLHelper.GetNodeValue(aitFile, "SalesOrder", "OrderNo");
            Dictionary<string, string> invalidProducts = new Dictionary<string, string>();
            foreach (var l in ordLines)
            {
                OrderLineElement line = new OrderLineElement();

                line.LineNo = l.Element("LineNo") != null ? l.Element("LineNo").Value : string.Empty;
                line.UnitOfMeasure = l.Element("UnitOfMeasure") != null ? l.Element("UnitOfMeasure").Value : string.Empty;
                line.PackageUnit = l.Element("UnitOfMeasure") != null ? l.Element("UnitOfMeasure").Value : string.Empty;
                line.OrderQty = l.Element("OrderQty") != null ? Convert.ToDecimal(l.Element("OrderQty").Value) : 0;
                line.OrderQtySpecified = true;
                line.UnitPrice = l.Element("UnitPrice") != null ? !string.IsNullOrEmpty(l.Element("UnitPrice").Value) ? Convert.ToDecimal(l.Element("UnitPrice").Value) : 0 : 0;
                line.UnitPriceSpecified = true;
                line.UnitDiscount = l.Element("UnitDiscount") != null ? !string.IsNullOrEmpty(l.Element("UnitDiscount").Value) ? Convert.ToDecimal(l.Element("UnitDiscount").Value) : 0 : 0;
                line.ExtendedPrice = l.Element("ExtendedPrice") != null ? !string.IsNullOrEmpty(l.Element("ExtendedPrice").Value) ? Convert.ToDecimal(l.Element("ExtendedPrice").Value) : 0 : 0;

                ProductElement product = new ProductElement
                {
                    Code = l.Element("Product").Element("Code") != null ? l.Element("Product").Element("Code").Value : string.Empty,
                    Barcode = l.Element("Product").Element("Barcode") != null ? l.Element("Product").Element("Barcode").Value : string.Empty,
                    Description = l.Element("Product").Element("Description") != null ? l.Element("Product").Element("Description").Value : string.Empty
                };
                SatelliteData.Models.Product prodCheck = new SatelliteData.Models.Product
                {
                    PW_BARCODE = product.Barcode,
                    PW_PARTNUM = product.Code,
                    PW_PRODUCTNAME = product.Description
                };
                if (ValidProduct(prodCheck))
                {
                    line.Product = product;
                    _orderLines.Add(line);
                }
                else
                {
                    validOrder = false;
                    if (!invalidProducts.ContainsKey(prodCheck.PW_PARTNUM))
                    {
                        invalidProducts.Add(prodCheck.PW_PARTNUM, prodCheck.PW_PRODUCTNAME);
                    }
                    var m = MethodBase.GetCurrentMethod().Name;
                    LogError(product.Code, "Order " + ordNo + " import Failed. Missing Product", m, DateTime.Now, "Product:" + prodCheck.PW_PARTNUM + "(" + prodCheck.PW_PRODUCTNAME + ")   does not exist in Cargowise", string.Empty);


                }

            }
            if (validOrder)
            {
                return _orderLines;
            }
            else
            {
                string errorMessage = "There were invalid products in the Order (" + ordNo + ")" + Environment.NewLine +
                        "The Following products are Invalid: " + Environment.NewLine;
                foreach (var k in invalidProducts)
                {
                    errorMessage += "Product: " + k.Key + " Product Name: " + k.Value + Environment.NewLine;
                }
                using (MailModule mail = new MailModule(this._mailServerSettings))
                {
                    mail.SendMsg(string.Empty, this._notify, "Missing Products for Order " + ordNo, errorMessage);
                }
                return null;
            }
        }

        private bool ValidProduct(SatelliteData.Models.Product product)
        {
            Type dbHelper = typeof(SatelliteDBHelper);
            var strArgs = new Type[] { typeof(string) };
            ConstructorInfo ctorHelper = dbHelper.GetConstructor(strArgs);
            var objHelper = ctorHelper.Invoke(new object[] { this._connString });
            var methHelper = dbHelper.GetMethod("ValidProduct");
            var retObject = methHelper.Invoke(objHelper, new object[] { product });
            if (retObject != null)
            {
                return (bool)retObject;
            }
            return false;
        }

        private List<DateElement> GetDatesFromAITOrder(XDocument aitFile)
        {
            var dateListing = aitFile.Descendants("SalesOrder").Elements("Dates").Elements();
            List<DateElement> _dates = new List<DateElement>();
            foreach (var date in dateListing)
            {
                DateElement newDate = new DateElement();
                newDate.DateType = (DateElementDateType)Enum.Parse(typeof(DateElementDateType), date.Element("DateType").Value);
                newDate.ActualDate = date.Element("ActualDate") != null ? date.Element("ActualDate").Value : null;
                newDate.EstimateDate = date.Element("EstimateDate") != null ? date.Element("EstimateDate").Value : null;
                _dates.Add(newDate);
            }
            return _dates;
        }

        private List<CompanyElement> GetCompanyListFromOrderFile(XDocument aitFile)
        {
            var CompanyListing = aitFile.Descendants("SalesOrder").Elements("Companies").Elements();
            List<CompanyElement> _companies = new List<CompanyElement>();
            foreach (var company in CompanyListing)
            {
                CompanyElement newCompany = new CompanyElement();
                newCompany.CompanyName = company.Element("CompanyName") != null ? company.Element("CompanyName").Value : string.Empty;
                switch (company.Element("CompanyType").Value)
                {
                    case "DeliveryAddress":
                        newCompany.CompanyType = CompanyElementCompanyType.OrderDeliveryAddress;
                        break;
                    case "BillToAddress":
                        newCompany.CompanyType = CompanyElementCompanyType.BillToAddress;
                        break;
                }
                newCompany.CompanyCode = company.Element("CompanyCode") != null ? company.Element("CompanyCode").Value : string.Empty;
                newCompany.Address1 = company.Element("Address1") != null ? company.Element("Address1").Value : string.Empty;
                newCompany.Address2 = company.Element("Address2") != null ? company.Element("Address2").Value : string.Empty;
                newCompany.Address3 = company.Element("Address3") != null ? company.Element("Address3").Value : string.Empty;
                newCompany.City = company.Element("City") != null ? company.Element("City").Value : string.Empty;
                newCompany.PostCode = company.Element("PostCode") != null ? company.Element("PostCode").Value : string.Empty;
                newCompany.State = company.Element("State") != null ? company.Element("State").Value : string.Empty;
                newCompany.PhoneNo = company.Element("PhoneNo") != null ? company.Element("PhoneNo").Value : string.Empty;
                newCompany.Country = company.Element("Country") != null ? company.Element("Country").Value : string.Empty;
                _companies.Add(newCompany);
            }
            _companies.Add(new CompanyElement
            {
                CompanyType = CompanyElementCompanyType.Consignor,
                CompanyOrgCode = "AUSINTSYD1"
            });
            return _companies;
        }
    }
}
