﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace XML_Locker.Cargowise
{

    public static class CWFunctions
    {

        public static string GetRef(List<XMLLocker.CW.AdditionalReference> additionalReferenceCollection, string v)
        {
            string result = string.Empty;
            var ar = (from r in additionalReferenceCollection
                      where (r.Type.Code == v)
                      select r.ReferenceNumber).FirstOrDefault();
            result = ar != null ? ar : result;
            return result;

        }

        public static bool IsCargowiseFile(string fileName)
        {
            bool cwFile = false;
            try
            {
                XDocument cwXML = XDocument.Load(fileName);
                var ns = cwXML.Root.Name.Namespace;
                if (ns.NamespaceName.Contains("www.cargowise.com") || ns.NamespaceName.Contains("www.edi.com"))
                {
                    cwFile = true;
                }

            }
            catch (Exception)
            {

            }
            return cwFile;
        }

        public static XNamespace GetNameSpace(string fileName)
        {
            XDocument doc = XDocument.Load(fileName);
            var ns = doc.Root.Name.Namespace;
            if (ns != null)
            {
                return ns;
            }
            return null;
        }
        public static string GetCodefromCW(string fileName, string codeTofind)
        {
            XDocument cwXML = XDocument.Load(fileName);
            var ns = cwXML.Root.Name.Namespace;
            string n = GetApplicationCode(ns.NamespaceName);

            string val = string.Empty;
            switch (n)
            {
                case "UDM":
                    var code = from c in cwXML.Descendants()
                               where c.Name.LocalName == codeTofind
                               select c;
                    val = (from v in code.Elements()
                           where v.Name.LocalName == "Code"
                           select v.Value).FirstOrDefault();
                    break;
                case "NDM":

                    break;
                case "XMS":

                    break;
                default:

                    break;
            }
            return val;
        }


        public static string GetXMLType(string messageFilePath)
        {
            string result = string.Empty;
            XDocument xDoc;
            xDoc = XDocument.Load(messageFilePath);
            //using (var fileStream = GetFileAsStream(messageFilePath))
            //{
            //    using (var reader = XmlReader.Create(fileStream))
            //    {
            //        xDoc = XDocument.Load(reader);
            //    }
            //}
            if (IsCargowiseFile(messageFilePath))
            {
                var nsList = xDoc.Descendants().Select(n => n.Name.NamespaceName).Distinct().ToList();
                XElement typeXML = null;
                foreach (XNamespace ns in nsList)
                {
                    typeXML = xDoc.Descendants().Where(n => n.Name == ns + "Body").FirstOrDefault();
                    if (typeXML != null)
                    {
                        break;
                    }
                    else
                    {
                        typeXML = xDoc.Descendants().Where(n => n.Name == ns + "InterchangeInfo").FirstOrDefault();
                    }

                }

                var name = (XElement)typeXML.FirstNode;

                switch (name.Name.LocalName)
                {
                    case "UniversalEvent":
                        result = "UDM";
                        break;
                    case "UniversalShipment":
                        result = "UDM";
                        break;
                    case "Native":
                        result = "NDM";
                        break;
                }
                return result;
            }
            else
            {
                return "N/A";
            }



        }
        public static string GetApplicationCode(string messageNamespace)
        {
            string appCode = string.Empty;
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    appCode = "UDM";
                    break;

                case "http://www.cargowise.com/Schemas/Native":
                    appCode = "NDM";
                    break;

                case "http://www.edi.com.au/EnterpriseService/":
                    appCode = "XMS";
                    break;

                default:
                    appCode = "";
                    break;
            }

            return appCode;


        }






    }
}
