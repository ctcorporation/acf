﻿using System;

namespace ACF_Satellite
{
    public class Customer
    {
        public string CustomerName { get; set; }
        public string Code { get; set; }
        public Guid Id { get; set; }
        public string ShortName { get; set; }

    }

}
