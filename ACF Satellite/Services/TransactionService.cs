﻿using SatelliteData.Models;
using SatelliteData.UnitOfWork;

using System;

namespace ACF_Satellite.Services
{
    class TransactionService : ITransactionService
    {
        public int AddTransaction(Transaction trans)
        {

            using (ISatelliteUnitOfWork unitOfWork = new SatelliteUnitOfWork(new SatelliteData.Models.SatelliteEntity()))
            {
                unitOfWork.Transactions.Add(trans);
                return unitOfWork.Complete();
            }
        }


        public int UpdateTransaction(Transaction transaction)
        {
            throw new NotImplementedException();
        }
    }
}
