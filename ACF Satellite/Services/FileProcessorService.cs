﻿using NodeData;
using SatelliteData.Models;
using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace ACF_Satellite.Services
{
    public class FileProcessorService : IFileProcessorService
    {

        private string _outputDir;
        private string _custRef;
        private string _xmlFile;
        private Guid _profID;
        private Transaction _result;
        private string _orderNo;

        public FileProcessorService(string output, string xmlFile, Guid profID)
        {
            _outputDir = output;
            _xmlFile = xmlFile;
            _profID = profID;
            _result = new Transaction();
        }

        public FileProcessorService(string output, string xmlFile)
        {
            _outputDir = output;
            _xmlFile = xmlFile;
            _result = new Transaction();
        }
        bool CreateCWFile(XMLLocker.Cargowise.XUS.UniversalInterchange cwFile, string recipient, string sender)
        {
            try
            {
                String cwXML = Path.Combine(Globals.OutputLocation, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(_outputDir, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + "-" + iFileCount + ".xml");
                }
                _orderNo = !string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.Order.OrderNumber) ? cwFile.Body.BodyField.Shipment.Order.OrderNumber : null;
                _custRef = !string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.Order.ClientReference) ? cwFile.Body.BodyField.Shipment.Order.ClientReference : null;
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                using (StringWriter writer = new StringWriter())
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.XUS.UniversalInterchange));
                    var cwNSUniversal = new XmlSerializerNamespaces();
                    cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                    xSer.Serialize(outputCW, cwFile, cwNSUniversal);
                    outputCW.Flush();
                    outputCW.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                _result.T_LOGMSG = "Error Found Creating Cargowise File. " + ex.GetType().Name + ": " + m.Name + ": " + ex.Message;
                _result.T_LOGTYPE = "Error";
                Global.LogError(recipient + "-" + sender, ex.GetType().Name, ex.Source, DateTime.Now, ex.ToLogString(ex.StackTrace), ex.TargetSite.Name);
                return false;
            }
        }

        public Transaction ProcessFile()
        {
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.CTCNodeConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                MethodBase m = MethodBase.GetCurrentMethod();
                heartBeat.RegisterHeartBeat(Globals.CustCode, m.Name, m.GetParameters());
            }
            var prof = Global.GetProfile(_profID);
            _result.T_FILENAME = _xmlFile;
            _result.T_DATETIME = DateTime.Now;
            if (prof != null)
            {
                _result.T_MSGTYPE = prof.P_MSGTYPE;
                _result.T_P = prof.P_ID;
                if (!string.IsNullOrEmpty(prof.P_METHOD))
                {
                    SatelliteProcessors sp = new SatelliteProcessors(Globals.ConnString.ConnString, Globals.AppLogPath, !string.IsNullOrEmpty(prof.P_NOTIFYEMAIL) ? prof.P_NOTIFYEMAIL : "csr@ctcorp.com.au", Globals.MailServerSettings);
                    var newfile = sp.AITToCommon(prof, _xmlFile);
                    if (newfile == null)
                    {
                        _result.T_LOGMSG = sp.ErrMsg;
                        _result.T_LOGTYPE = "Error";
                        return _result;
                    }

                    if (newfile.GetType() == typeof(XMLLocker.Cargowise.XUS.UniversalInterchange))
                    {
                        CreateCWFile((XMLLocker.Cargowise.XUS.UniversalInterchange)newfile, prof.P_RECIPIENTID, prof.P_SENDERID);
                        _result.T_LOGMSG = "File Converted Successfully";
                        _result.T_LOGTYPE = "Success";
                        return _result;
                    }
                }
            }
            else
            {
                _result.T_LOGMSG = "Unable to process file. No Profile file found: " + _xmlFile;
                _result.T_LOGTYPE = "No Profile";
                return _result;
            }
            if (_orderNo != null)
            {
                _result.T_REF1 = _orderNo;
                _result.T_REF1TYPE = "Order No";
            }
            if (_custRef != null)
            {
                _result.T_REF2 = _custRef;
                _result.T_REF2TYPE = "Cust Order";
            }
            return _result;
        }
    }
}
