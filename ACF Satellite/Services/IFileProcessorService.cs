﻿using SatelliteData.Models;

namespace ACF_Satellite.Services
{
    public interface IFileProcessorService
    {
        Transaction ProcessFile();
    }
}