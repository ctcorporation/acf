﻿using SatelliteData.Models;

namespace ACF_Satellite.Services
{
    public interface ITransactionService
    {

        int AddTransaction(Transaction transaction);

        int UpdateTransaction(Transaction transaction);

    }
}
