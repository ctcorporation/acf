﻿using SatelliteData;
using SatelliteData.Models;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace ACF_Satellite
{
    public partial class frmNewCustomer : Form
    {

        public string CustCode { get; set; }
        public string CustPath { get; set; }
        public string CustShortName { get; set; }
        public frmNewCustomer()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ConnectionManager conn = new ConnectionManager();

            using (SatelliteEntity db = new SatelliteEntity(conn.ConnString))
            {
                try
                {


                    var cust = (from c in db.Customers
                                select c).FirstOrDefault();
                    if (cust != null)
                    {
                        cust.C_CODE = edCustCode.Text;
                        cust.C_PATH = edRootPath.Text;
                        cust.C_NAME = edCustomerName.Text;
                        cust.C_ON_HOLD = cbHold.Checked ? "Y" : "N";
                        cust.C_IS_ACTIVE = cbActive.Checked ? "Y" : "N";
                        cust.C_SHORTNAME = edShortName.Text;
                    }
                    else
                    {
                        cust = new SatelliteData.Models.Customer
                        {
                            C_CODE = edCustCode.Text,
                            C_PATH = edRootPath.Text,
                            C_SHORTNAME = edShortName.Text,
                            C_IS_ACTIVE = cbActive.Checked ? "Y" : "N",
                            C_ON_HOLD = cbHold.Checked ? "Y" : "N",
                            C_FTP_CLIENT = cbFtpClient.Checked ? "Y" : "N",
                            C_NAME = edCustomerName.Text,
                            C_TRIAL = cbTrial.Checked ? "Y" : "N"
                        };
                        db.Customers.Add(cust);
                    }
                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Saving Record : " + ex.Message);
                }
            }
            btnSave.Text = "&Save";

        }

        private void GetCustomer(string custCode)
        {
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var cust = (from c in db.Customers
                            where c.C_CODE == custCode
                            select c).FirstOrDefault();
                if (cust != null)
                {
                    edCustCode.Text = cust.C_CODE;
                    edCustomerName.Text = cust.C_NAME;
                    edShortName.Text = cust.C_SHORTNAME;
                    edRootPath.Text = cust.C_PATH;
                    cbActive.Checked = cust.C_IS_ACTIVE == "Y" ? true : false;
                    cbHold.Checked = cust.C_ON_HOLD == "Y" ? true : false;
                    cbFtpClient.Checked = cust.C_FTP_CLIENT == "Y" ? true : false;
                }
            }
            {
                btnSave.Text = "&Save";
                edRootPath.Text = Globals.PrimaryLocation;
                edCustCode.Text = Globals.Customer.Code;

            }

        }

        private void frmNewCustomer_Load(object sender, EventArgs e)
        {
            if (Globals.Customer != null)
            {
                GetCustomer(Globals.Customer.Code.ToUpper().Trim());
            }
            else
            {
                edCustCode.Text = CustCode;
                try
                {
                    edRootPath.Text = Path.Combine(CustPath, CustCode);
                }
                catch (Exception)
                {

                }

                edShortName.Text = CustShortName;
            }

        }
    }
}
