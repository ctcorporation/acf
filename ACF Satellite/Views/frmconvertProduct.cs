﻿using NodeResources;
using SatelliteData;
using SatelliteData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using XMLLocker.CTC;

namespace ACF_Satellite.Views
{
    public partial class frmConvertProduct : Form
    {
        DataTable tableMapping;
        public frmConvertProduct()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            NodeFile nodeFile = ConvertToTable();
            ToCargowise commonToCW = new ToCargowise(Globals.AppLogPath);
            commonToCW.CreateProductFile(nodeFile, XMLLocker.Cargowise.NDM.Action.MERGE, txtOutputFolder.Text);
            MessageBox.Show("Products Imported and Database updated");

        }

        private bool ValidProduct(SatelliteData.Models.Product product)
        {
            Type dbHelper = typeof(SatelliteDBHelper);
            var strArgs = new Type[] { typeof(string) };
            ConstructorInfo ctorHelper = dbHelper.GetConstructor(strArgs);
            var objHelper = ctorHelper.Invoke(new object[] { Globals.ConnString.ConnString });
            var methHelper = dbHelper.GetMethod("ValidProduct");
            var retObject = methHelper.Invoke(objHelper, new object[] { product });
            if (retObject != null)
            {
                return (bool)retObject;
            }
            return false;
        }

        private NodeFile ConvertToTable()
        {
            FileInfo fi = new FileInfo(txtFileToConvert.Text);
            DataTable tb = new DataTable();
            switch (fi.Extension.ToUpper())
            {
                case ".XLS":
                    tb = ExcelToTable.ToTable(fi.FullName);
                    break;
                case ".XLSX":
                    tb = ExcelToTable.ToTable(fi.FullName);
                    break;
                case ".CSV":
                    tb = CsvHelper.ConvertCSVtoDataTable(fi.FullName);

                    break;

            }
            List<XMLLocker.MapOperation> targetMapping = new List<XMLLocker.MapOperation>();
            foreach (DataRow row in tableMapping.Rows)
            {
                XMLLocker.MapOperation map = new XMLLocker.MapOperation
                {
                    MD_FromField = row["MapFrom"].ToString(),
                    MD_ToField = row["MapTo"].ToString(),
                    MD_DataType = row["DataType"].ToString()
                };
                targetMapping.Add(map);
            }
            var result = NodeFunctions.ImportProducts(tb, txtOwner.Text, txtCustomer.Text, targetMapping, Globals.AppLogPath);
            return result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();

            fd.ShowDialog();
            txtFileToConvert.Text = fd.FileName;
            GetFieldNames(txtFileToConvert.Text);



        }

        private void GetFieldNames(string text)
        {
            FileInfo fi = new FileInfo(txtFileToConvert.Text);
            DataTable tb = new DataTable();
            switch (fi.Extension.ToUpper())
            {
                case ".XLS":
                    tb = ExcelToTable.ToTable(fi.FullName);
                    break;
                case ".XLSX":
                    tb = ExcelToTable.ToTable(fi.FullName);
                    break;
                case ".CSV":
                    CheckForCharacters(fi.FullName, "\"");
                    tb = CsvHelper.ConvertCSVtoDataTable(fi.FullName);

                    break;

            }
            lvFieldList.Items.Clear();

            foreach (DataColumn dc in tb.Columns)
            {
                lvFieldList.Items.Add(dc.ColumnName);
            }

        }

        private void CheckForCharacters(string fullName, string charToReplace)
        {
            var txt = File.ReadAllText(fullName);
            var newTxt = txt.Replace(charToReplace, "");
            File.WriteAllText(fullName, newTxt);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtOutputFolder.Text;
            fd.ShowDialog();
            txtOutputFolder.Text = fd.SelectedPath;

        }

        private void frmConvertProductData_Load(object sender, EventArgs e)
        {
            FillConvertionTypes();
            txtOutputFolder.Text = Globals.OutputLocation;
            tableMapping = new DataTable();
            tableMapping.Columns.Add("ConvertType", typeof(string));
            tableMapping.Columns.Add("MapFrom", typeof(string));
            tableMapping.Columns.Add("MapTo", typeof(string));
            tableMapping.Columns.Add("DataType", typeof(string));
            dgMapping.DataSource = tableMapping;
        }

        private void FillConvertionTypes()
        {
            cmbConversion.DisplayMember = "Text";
            cmbConversion.ValueMember = "Value";
            cmbConversion.Items.Clear();
            var items = new[] {
         new { Text = "(none selected)", Value = "N/A" },
         new { Text = "Native Product (Cargowise)", Value = "PROD" },
         new { Text = "Organisation (Cargowise)", Value = "COMP" }
        };
            cmbConversion.DataSource = items;
            cmbDataTypes.Items.Clear();
            cmbDataTypes.DisplayMember = "Text";
            cmbDataTypes.ValueMember = "Value";
            var dataItems = new[]
            {
                new {Text = "(none selected)", Value="N/A"},
                new {Text = "Boolean", Value="BOO"},
                new {Text = "Date/Time", Value="DAT"},
                new {Text = "Decimal", Value="NUM"},
                new {Text = "Integer", Value="INT"},
                new {Text = "String", Value="STR"}
            };
            cmbDataTypes.DataSource = dataItems;

        }

        private void cmbConversion_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbConversion.SelectedValue)
            {
                case "PROD":
                    FillProductFields();
                    break;

                case "COMP":
                    FillOrgFields();
                    break;


            }
            using (SatelliteEntity _context = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var mapList = (from m in _context.MapOperations
                               where (m.MD_Type == cmbConversion.SelectedValue.ToString())
                               orderby m.MD_MapDescription
                               select new { m.MD_MapDescription })
                               .Distinct()
                               .ToList();

                cmbMapList.DisplayMember = "MD_MapDescription";

                if (mapList != null)
                {
                    mapList.Insert(0, new { MD_MapDescription = "(none selected)" });
                    cmbMapList.DataSource = mapList;


                }
            }
        }

        private void FillOrgFields()
        {
            lvRequiredFields.Items.Clear();
            lvRequiredFields.Items.Add("Company Name");
            lvRequiredFields.Items.Add("Company Code");
            lvRequiredFields.Items.Add("Address 1");
            lvRequiredFields.Items.Add("Address 2");
            lvRequiredFields.Items.Add("City");
            lvRequiredFields.Items.Add("State");
            lvRequiredFields.Items.Add("Post Code");
            lvRequiredFields.Items.Add("Country");
            lvRequiredFields.Items.Add("Country Code");

        }

        private void FillProductFields()
        {
            lvRequiredFields.Items.Clear();
            lvRequiredFields.Items.Add("Code");
            lvRequiredFields.Items.Add("Description");
            lvRequiredFields.Items.Add("Barcode");
            lvRequiredFields.Items.Add("Height");
            lvRequiredFields.Items.Add("Width");
            lvRequiredFields.Items.Add("Depth");
            lvRequiredFields.Items.Add("Weight");
            lvRequiredFields.Items.Add("WeightUnit");
            lvRequiredFields.Items.Add("DimsUnit");
        }

        private void btnAddRequiredField_Click(object sender, EventArgs e)
        {

            AddRequiredField();


        }

        private void AddRequiredField()
        {
            if (cmbConversion.SelectedValue.ToString() != "N/A")
            {
                var currentLabel = btnAddMapping.Text.Split(':');
                if (currentLabel.Length == 2)
                {
                    currentLabel[1] = lvRequiredFields.SelectedItem.ToString();
                    btnAddMapping.Text = currentLabel[0] + " : " + currentLabel[1];
                }

                else
                {
                    btnAddMapping.Text = " : " + lvRequiredFields.SelectedItem.ToString();
                }
            }
        }

        private void btnAddField_Click(object sender, EventArgs e)
        {
            AddField();
        }

        private void AddField()
        {
            if (cmbConversion.SelectedValue.ToString() != "N/A")
            {
                var currentLabel = btnAddMapping.Text.Split(':');
                if (currentLabel.Length == 2)
                {
                    currentLabel[0] = lvFieldList.SelectedItem.ToString();
                    btnAddMapping.Text = currentLabel[0] + " : " + currentLabel[1];
                }

                else
                {
                    btnAddMapping.Text = lvFieldList.SelectedItem.ToString() + " : ";
                }
            }
        }

        private void btnAddMapping_Click(object sender, EventArgs e)
        {
            if (cmbDataTypes.SelectedItem.ToString() != "N/A")
            {
                if (btnAddMapping.Text.Split(':').Count() == 2)
                {
                    DataRow dr = tableMapping.NewRow();
                    dr["ConvertType"] = cmbConversion.SelectedValue;
                    var currentLabel = btnAddMapping.Text.Split(':');
                    if (currentLabel.Length == 2)
                    {
                        dr["MapFrom"] = currentLabel[1];
                        dr["MapTo"] = currentLabel[0];
                        dr["DataType"] = cmbDataTypes.SelectedValue.ToString();
                        tableMapping.Rows.Add(dr);
                        foreach (var item in lvRequiredFields.Items.Cast<string>().ToList())
                        {
                            if (item == currentLabel[1].Trim())
                            {
                                lvRequiredFields.Items.Remove(item);
                            }
                        }
                        foreach (var item in lvFieldList.Items.Cast<string>().ToList())
                        {
                            if (item == currentLabel[0].Trim())
                            {
                                lvFieldList.Items.Remove(item);
                            }
                        }
                    }
                }
            }


        }

        private void btnSaveMapping_Click(object sender, EventArgs e)
        {
            if (cmbConversion.SelectedValue.ToString() != "N/A")
            {
                if (string.IsNullOrEmpty(txtMapDescription.Text))
                {
                    MessageBox.Show("You cannot have a blank Map Description", "Save Mapping Operation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                using (SatelliteEntity _context = new SatelliteEntity(Globals.ConnString.ConnString))
                {
                    var map = (from m in _context.MapOperations
                               where (m.MD_MapDescription.ToLower() == txtMapDescription.Text.ToLower())
                               && (m.MD_Type == cmbConversion.SelectedValue.ToString())
                               select m).ToList();

                    if (map.Count > 0)
                    {
                        foreach (var m in map)
                        {
                            _context.MapOperations.Remove(m);
                        }
                    }

                    foreach (DataRow row in tableMapping.Rows)
                    {

                        MapOperation md = new MapOperation
                        {
                            MD_MapDescription = txtMapDescription.Text,
                            MD_Type = cmbConversion.SelectedValue.ToString(),
                            MD_FromOrgCode = txtOwner.Text,
                            MD_ToOrgCode = txtCustomer.Text,
                            MD_FromField = row["MapFrom"].ToString(),
                            MD_ToField = row["MapTo"].ToString(),
                            MD_DataType = row["DataType"].ToString()
                        };
                        _context.MapOperations.Add(md);
                    }
                    _context.SaveChanges();
                }
            }
            else
            {
                MessageBox.Show("Please ensure you have selected a Data Type before adding to the mapping", "Missing Data Type selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnLoadMapping_Click(object sender, EventArgs e)
        {
            using (SatelliteEntity _context = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var map = (from m in _context.MapOperations
                           where (m.MD_MapDescription.ToLower() == cmbMapList.Text.ToLower())
                            && (m.MD_Type == cmbConversion.SelectedValue.ToString())
                           select m).ToList();
                if (map.Count > 0)
                {
                    tableMapping.Rows.Clear();
                    foreach (var m in map)
                    {
                        DataRow row = tableMapping.NewRow();
                        row["ConvertType"] = m.MD_Type;
                        row["MapFrom"] = m.MD_FromField;
                        row["MapTo"] = m.MD_ToField;
                        row["DataType"] = m.MD_DataType;
                        tableMapping.Rows.Add(row);
                        txtCustomer.Text = m.MD_ToOrgCode;
                        txtOwner.Text = m.MD_FromOrgCode;
                    }

                }
                dgMapping.Refresh();
            }
        }

        private void lvFieldList_DoubleClick(object sender, EventArgs e)
        {
            AddField();
        }

        private void lvRequiredFields_DoubleClick(object sender, EventArgs e)
        {
            AddRequiredField();
        }

        private void dgMapping_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {

        }

        private void dgMapping_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var field = e.Row.Cells[2].Value.ToString().Trim();
            var required = e.Row.Cells[1].Value.ToString().Trim();

            if (!lvRequiredFields.Items.Contains(required))
            {
                lvRequiredFields.Items.Add(required);
            }

            if (!lvFieldList.Items.Contains(field))
            {
                lvFieldList.Items.Add(field);
            }
        }
        private void btnImportOnly_Click(object sender, EventArgs e)
        {
            NodeFile nodeFile = ConvertToTable();
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                foreach (var product in nodeFile.ProductImport)
                {

                    SatelliteData.Models.Product prod = new SatelliteData.Models.Product
                    {
                        PW_BARCODE = product.Barcode,
                        PW_PARTNUM = product.Code,
                        PW_PRODUCTNAME = product.Description,
                        PW_UOM = product.StockKeepingUnit
                    };
                    if (!ValidProduct(prod))
                    {
                        db.Products.Add(prod);
                    }

                }
                db.SaveChanges();


            }
            MessageBox.Show("Products Imported successfully.", "Import and Convert", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnBulkExport_Click(object sender, EventArgs e)
        {
            NodeFile nodeFile = new NodeFile
            {
                IdendtityMatrix = new NodeFileIdendtityMatrix
                {
                    CustomerId = txtCustomer.Text,
                    SenderId = txtOwner.Text,
                    DocumentIdentifier = "Product Import"
                }
            };
            List<ProductElement> products = new List<ProductElement>();
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {


                var productlist = db.Set<SatelliteData.Models.Product>().OrderBy(x => x.PW_PARTNUM).ToList();
                foreach (var product in productlist)
                {
                    var prod = new ProductElement
                    {
                        Barcode = product.PW_BARCODE,
                        Code = product.PW_PARTNUM,
                        Description = product.PW_PRODUCTNAME

                    };
                    products.Add(prod);
                }

            }
            nodeFile.ProductImport = products.ToArray();
            ToCargowise commonToCW = new ToCargowise(Globals.AppLogPath);
            commonToCW.CreateProductFile(nodeFile, XMLLocker.Cargowise.NDM.Action.MERGE, txtOutputFolder.Text);
            MessageBox.Show("Products Imported and Database updated");
        }
    }
}
