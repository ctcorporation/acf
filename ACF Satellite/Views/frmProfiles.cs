﻿using SatelliteData.Models;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace ACF_Satellite
{
    public partial class frmProfiles : Form
    {
        public Guid r_id;
        public Guid gP_id;


        public frmProfiles()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeliveryChecked(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            String rbt = rb.Tag.ToString();
            switch (rbt)
            {
                case "pickup":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = false;
                    edServerAddress.Visible = false;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;


                    break;
                case "eadapter":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "eAdapter URL";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                case "ftp":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = true;
                    lblFTPReceivePath.Visible = true;

                    break;
                case "email":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = true;
                    edServerAddress.Visible = true;
                    lblServerAddress.Text = "Email Address";
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                default:
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
            }
        }


        private void frmProfiles_Load(object sender, EventArgs e)
        {

            if (Globals.Customer == null)
            {
                frmNewCustomer NewCustomer = new frmNewCustomer();
                if (NewCustomer.ShowDialog(this) == DialogResult.OK)
                {
                    frmProfiles_Load(sender, e);
                }
            }
            else
            {
                FillGrid(Globals.Customer.Id);
                ddlDTSType.DisplayMember = "Text";
                ddlDTSType.ValueMember = "Value";

                var items = new[] {
            new { Text = "Replace", Value = "R" },
            new { Text = "Update", Value = "U" },
            new { Text = "Delete", Value = "D" },
            new { Text = "Modify", Value = "M" }};
                ddlDTSType.DataSource = items;
            }
        }

        private void FillGrid(Guid C_id)
        {
            if (C_id != Guid.Empty)
            {
                using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
                {
                    var profList = (from p in db.Profiles                                    
                                    orderby p.P_DESCRIPTION
                                    select p).ToList();

                    if (profList.Count > 0)
                    {
                        if (cbHideInactive.Checked)
                        {
                            profList = profList.Where(x => x.P_ACTIVE == "Y").ToList();
                        }
                        dgProfile.AutoGenerateColumns = false;
                        dgProfile.DataSource = profList;
                    }
                }
            }
        }

        private void rbCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void rbVendor_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void ReadOnlycontrols(Boolean toggle)
        {
            if (!toggle)
            {

                cbDTS.Enabled = true;

            }
            else
            {

                cbDTS.Enabled = false;
            }
        }

        void SetupReceive()
        {
            String strReceive = String.Empty;
            foreach (RadioButton rb in gbReceiveMethod.Controls)
            {
                if (rb.Checked)
                {
                    switch (rb.Name)
                    {


                        case "rbPickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblFTPReceivePath.Visible = false;
                            edFTPReceivePath.Visible = false;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            btnReceiveFolder.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;

                            break;
                        case "rbReceiveFTP":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = true;
                            edFTPReceivePath.Visible = true;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            btnReceiveFolder.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;
                            break;

                        case "rbEmail":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = false;
                            edFTPReceivePath.Visible = false;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            btnReceiveFolder.Visible = false;
                            lblReceiveEmailAddress.Visible = true;
                            edReceiveEmailAddress.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            edReceiveCustomerName.Visible = true;

                            cbSSL.Visible = true;
                            break;
                        case "rbReceivePickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblFTPReceivePath.Visible = true;
                            edFTPReceivePath.Visible = true;
                            btnReceiveFolder.Visible = true;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;
                            break;

                    }
                }
            }
        }

        void SetupSend()
        {
            String strSend = String.Empty;

            foreach (RadioButton rb in gbSendMethod.Controls)
            {

                if (rb.Checked == true)
                {

                    switch (rb.Name)
                    {
                        case "rbPickupSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Pickup Folder";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = true;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            break;
                        case "rbCTCSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Server Address";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            lblTestResults.Visible = true;
                            btnPickupFolder.Visible = false;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbSoapSend":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            lblTestResults.Visible = true;
                            btnPickupFolder.Visible = false;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = true;
                            lblSendSubject.Text = "Endpoint";
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbFTPSend":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            lblSendFTPPort.Visible = true;
                            break;
                        case "rbEmailSend":
                            lblSendServer.Visible = false;
                            edSendCTCUrl.Visible = false;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = true;
                            edSendEmailAddress.Visible = true;
                            lblSendSubject.Visible = true;
                            edSendSubject.Visible = true;
                            lblSendSubject.Text = "Subject";
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbFTPSendDirect":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = true;
                            lblSendFTPPort.Visible = true;
                            break;
                    }
                }

            }
        }

        private void FillDTSGrid(Guid pid)
        {
            try
            {
                using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
                {
                    var dtsList = (from d in db.DTS
                                   where d.D_P == pid
                                   orderby d.D_INDEX
                                   select d).ToList();
                    if (dtsList.Count > 0)
                    {
                        dgDts.DataSource = dtsList;
                    }
                    dgDts.AutoGenerateColumns = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);
            }
        }

        void FillCWContext()
        {
            try
            {
                using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
                {
                    var context = (from c in db.CargowiseContexts
                                   orderby c.CC_Context
                                   select new { Value = c.CC_ID, Data = c.CC_Context }).ToList();
                    if (context.Count > 0)
                    {
                        context.Add(new { Value = Guid.Empty, Data = "(none selected)" });
                        ddlCWContext.DataSource = context;
                        ddlCWContext.DisplayMember = "Data";
                        ddlCWContext.ValueMember = "Value";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);
            }
        }

        private void ClearControls(bool allControls)
        {
            if (allControls)
            {

            }
            tvwXSD.ResetText();
            edReceiveCustomerName.Text = "";
            edDescription.Text = "";
            edFTPReceivePort.Text = "";
            edReasonCode.Text = "";
            edRecipientID.Text = "";
            edReceivePassword.Text = "";
            edReceiveUsername.Text = "";
            edServerAddress.Text = "";
            btnAdd.Text = "&Add >>";
            edDTSCurrentValue.Text = "";
            rbReceive.Checked = false;
            rbSend.Checked = false;
            edReasonCode.Text = "";
            edEventCode.Text = "";
            cmbMsgType.SelectedIndex = -1;
            cbChargeable.Checked = false;
            cbGroupCharges.Checked = false;
            edServerAddress.Text = "";
            edFTPReceivePort.Text = "";
            edFTPReceivePath.Text = "";
            btnXsdPath.Text = "";
            edReceiveSubject.Text = "";
            edReceiveEmailAddress.Text = "";
            edReceiveSender.Text = "";
            edReceiveSubject.Text = "";
            edXSDPath.Text = "";
            edCustomMethod.Text = "";
            lbParameters.Items.Clear();
            edLibraryName.Text = "";
            edParameter.Text = "";
        }

        public void ConvertXMLNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);
            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "<" + xmlNode.Name + ">";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "atrribute: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;

            }
            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attrib in xmlNode.Attributes)
                {
                    ConvertXMLNodeToTreeNode(attrib, newTreeNode.Nodes);
                }
            }
            if (xmlNode.HasChildNodes)
            {
                foreach (XmlNode childNode in xmlNode.ChildNodes)
                {
                    ConvertXMLNodeToTreeNode(childNode, newTreeNode.Nodes);
                }
            }


        }

        private void ClearProfile()
        {
            ClearControls(false);
        }

        private void rbPickup_CheckedChanged(object sender, EventArgs e)
        {
            SetupReceive();
        }

        private void rbPickupSend_CheckedChanged(object sender, EventArgs e)
        {
            SetupSend();
        }

        private void btnXsdPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fs = new OpenFileDialog();
            try
            {
                fs.DefaultExt = "*.XSD";
                fs.FileName = edXSDPath.Text;
            }
            catch (Exception)
            { }
            fs.ShowDialog();
            edXSDPath.Text = fs.FileName;
        }

        private void btnLoadXSD_Click(object sender, EventArgs e)
        {

        }

        private void bbAddDTS_Click(object sender, EventArgs e)
        {
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var dts = (from d in db.DTS
                           where d.D_C == Globals.Customer.Id
                           && d.D_P == gP_id
                           && d.D_INDEX == int.Parse(edDTSIndex.Text)
                           select d).FirstOrDefault();
                if (dts == null)
                {
                    DTS newDts = new DTS
                    {
                        D_C = r_id,
                        D_INDEX = int.Parse(edDTSIndex.Text),
                        D_FILETYPE = ddlDTSFileType.Text,
                        D_DTSTYPE = (string)ddlDTSType.SelectedValue,
                        D_P = gP_id,
                        D_DTS = rbDTSRecord.Checked ? "R" : rbDTSStructure.Checked ? "S" : string.Empty,
                        D_SEARCHPATTERN = edDTSSearch.Text,
                        D_NEWVALUE = edDTSValue.Text,
                        D_QUALIFIER = edDTSQual.Text,
                        D_TARGET = edDTSTarget.Text,
                        D_CURRENTVALUE = edDTSCurrentValue.Text
                    };
                    db.DTS.Add(newDts);
                }
                else
                {
                    dts.D_FILETYPE = ddlDTSFileType.Text;
                    dts.D_DTS = rbDTSRecord.Checked ? "R" : rbDTSStructure.Checked ? "S" : string.Empty;
                    dts.D_SEARCHPATTERN = edDTSSearch.Text;
                    dts.D_DTSTYPE = (string)ddlDTSType.SelectedValue;
                    dts.D_NEWVALUE = edDTSValue.Text;
                    dts.D_QUALIFIER = edDTSQual.Text;
                    dts.D_TARGET = edDTSTarget.Text;
                    dts.D_CURRENTVALUE = edDTSCurrentValue.Text;
                }
                db.SaveChanges();
            }
            try
            {
                FillDTSGrid(gP_id);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving Record:" + ex.Message);
            }
        }

        private void btnPickupFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdPickup = new FolderBrowserDialog();
            fdPickup.SelectedPath = edSendCTCUrl.Text;
            fdPickup.ShowDialog();
            edSendCTCUrl.Text = fdPickup.SelectedPath;
        }

        private void btnReceiveFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edFTPReceivePath.Text;
            fd.ShowDialog();
            edFTPReceivePath.Text = fd.SelectedPath;
        }

        private void bbTestRx_Click(object sender, EventArgs e)
        {
            if (cbSSL.Checked)
            {
            }
            else
            {
            }

            txtTestResults.Text = "";
            //txtTestResults.Text = MailModule.CheckMail(edServerAddress.Text, Int16.Parse(edFTPReceivePort.Text), ssl, edReceiveUsername.Text, edReceivePassword.Text);
        }

        private void btnNewProfile_Click(object sender, EventArgs e)
        {
            ClearProfile();
            btnAdd.Text = "Add  >>";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string xsdPath = PutXSD(edXSDPath.Text);
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                try
                {
                    if (btnAdd.Text == "&Update >>")
                    {
                        var prof = (from p in db.Profiles
                                    where p.P_ID == gP_id
                                    select p).FirstOrDefault();
                        if (prof != null)
                        {
                            prof.P_REASONCODE = edReasonCode.Text.ToUpper();
                            prof.P_EVENTCODE = edEventCode.Text.ToUpper();
                            prof.P_DTS = cbDTS.Checked ? "Y" : "N";
                            prof.P_DIRECTION = rbSend.Checked ? "S" : rbReceive.Checked ? "R" : string.Empty;
                            prof.P_DESCRIPTION = edDescription.Text;
                            prof.P_RECIPIENTID = edRecipientID.Text;
                            prof.P_SENDERID = edSenderID.Text;
                            prof.P_CHARGEABLE = cbChargeable.Checked ? "Y" : "N";
                            prof.P_GROUPCHARGES = cbGroupCharges.Checked ? "Y" : "N";
                            prof.P_BILLTO = rbCustomer.Checked ? Globals.Customer.Code : edRecipientID.Text;
                            prof.P_MSGTYPE = cmbMsgType.Text;
                            prof.P_FILETYPE = cmbFileType.Text;
                            prof.P_SSL = cbSSL.Checked ? "Y" : "N";
                            prof.P_METHOD = edCustomMethod.Text;
                            prof.P_LIBNAME = !string.IsNullOrEmpty(edLibraryName.Text) ? edLibraryName.Text.Trim() : string.Empty;
                            prof.P_XSD = !string.IsNullOrEmpty(xsdPath) ? xsdPath : string.Empty;
                            prof.P_ACTIVE = cbProfileActive.Checked ? "Y" : "N";
                            prof.P_NOTIFY = cbNotify.Checked ? "Y" : "N";
                            prof.P_NOTIFYEMAIL = edNotifyEmail.Text;
                            prof.P_C = Globals.Customer.Id;
                            if (rbSend.Checked)
                            {
                                prof.P_PORT = edSendFtpPort.Text;
                                prof.P_SERVER = edSendCTCUrl.Text;
                                prof.P_USERNAME = edSendUsername.Text;
                                prof.P_PASSWORD = edSendPassword.Text;
                                prof.P_SUBJECT = edSendSubject.Text;
                                prof.P_PATH = edSendFolder.Text;
                                prof.P_EMAILADDRESS = edSendEmailAddress.Text;


                                string sDelivery = string.Empty;
                                foreach (RadioButton rb in gbSendMethod.Controls)
                                {
                                    if (rb.Checked)
                                    {
                                        switch (rb.Name)
                                        {

                                            case "rbPickupSend":
                                                {
                                                    sDelivery = "R";
                                                    break;
                                                }
                                            case "rbCTCSend":
                                                {
                                                    sDelivery = "C";
                                                    break;
                                                }
                                            case "rbFTPSend":
                                                {
                                                    sDelivery = "F";
                                                    break;
                                                }
                                            case "rbFTPSendDirect":
                                                {
                                                    sDelivery = "D";
                                                    break;
                                                }
                                            case "rbEmailSend":
                                                {
                                                    sDelivery = "E";
                                                    break;
                                                }
                                            case "rbSoapSend":
                                                {
                                                    sDelivery = "S";
                                                    break;
                                                }
                                        }
                                    }
                                }
                                prof.P_DELIVERY = sDelivery;
                            }
                            if (rbReceive.Checked)
                            {
                                prof.P_PORT = edFTPReceivePort.Text;
                                prof.P_SERVER = edServerAddress.Text;
                                prof.P_USERNAME = edReceiveUsername.Text;
                                prof.P_PASSWORD = edReceivePassword.Text;
                                prof.P_SUBJECT = edReceiveSubject.Text;
                                prof.P_PATH = edFTPReceivePath.Text;
                                prof.P_EMAILADDRESS = edReceiveEmailAddress.Text;
                                prof.P_CUSTOMERCOMPANYNAME = edReceiveCustomerName.Text;
                                prof.P_SENDEREMAIL = edReceiveSender.Text;
                                string sDelivery = string.Empty;
                                foreach (RadioButton rb in gbReceiveMethod.Controls)
                                {
                                    if (rb.Checked)
                                    {
                                        switch (rb.Name)
                                        {

                                            case "rbPickup":
                                                {
                                                    sDelivery = "R";

                                                    break;
                                                }
                                            case "rbSendCTC":
                                                {
                                                    sDelivery = "C";
                                                    break;
                                                }
                                            case "rbSendFTP":
                                                {
                                                    sDelivery = "F";
                                                    break;
                                                }
                                            case "rbEmail":
                                                {
                                                    sDelivery = "E";
                                                    break;
                                                }
                                            case "rbReceivePickup":
                                                {
                                                    sDelivery = "P";
                                                    break;
                                                }
                                        }
                                    }
                                }
                                prof.P_DELIVERY = sDelivery;
                            }
                        }
                    }
                    else
                    {
                        Profile profile = new Profile
                        {
                            P_REASONCODE = edReasonCode.Text.ToUpper(),
                            P_EVENTCODE = edEventCode.Text.ToUpper(),
                            P_DTS = cbDTS.Checked ? "Y" : "N",
                            P_DIRECTION = rbSend.Checked ? "S" : rbReceive.Checked ? "R" : string.Empty,
                            P_DESCRIPTION = edDescription.Text,
                            P_RECIPIENTID = edRecipientID.Text,
                            P_SENDERID = edSenderID.Text,
                            P_CHARGEABLE = cbChargeable.Checked ? "Y" : "N",
                            P_GROUPCHARGES = cbGroupCharges.Checked ? "Y" : "N",
                            P_BILLTO = rbCustomer.Checked ? Globals.Customer.Code : edRecipientID.Text,
                            P_MSGTYPE = cmbMsgType.Text,
                            P_FILETYPE = cmbFileType.Text,
                            P_SSL = cbSSL.Checked ? "Y" : "N",
                            P_METHOD = edCustomMethod.Text,
                            P_LIBNAME = !string.IsNullOrEmpty(edLibraryName.Text) ? edLibraryName.Text.Trim() : string.Empty,
                            P_XSD = !string.IsNullOrEmpty(xsdPath) ? xsdPath : string.Empty,
                            P_ACTIVE = cbProfileActive.Checked ? "Y" : "N",
                            P_NOTIFY = cbNotify.Checked ? "Y" : "N",
                            P_NOTIFYEMAIL = edNotifyEmail.Text,
                            P_C = Globals.Customer.Id
                        };
                        if (rbSend.Checked)
                        {
                            profile.P_PORT = edSendFtpPort.Text;
                            profile.P_SERVER = edSendCTCUrl.Text;
                            profile.P_USERNAME = edSendUsername.Text;
                            profile.P_PASSWORD = edSendPassword.Text;
                            profile.P_SUBJECT = edSendSubject.Text;
                            profile.P_PATH = edSendFolder.Text;
                            profile.P_EMAILADDRESS = edSendEmailAddress.Text;


                            string sDelivery = string.Empty;
                            foreach (RadioButton rb in gbSendMethod.Controls)
                            {
                                if (rb.Checked)
                                {
                                    switch (rb.Name)
                                    {

                                        case "rbPickupSend":
                                            {
                                                sDelivery = "R";
                                                break;
                                            }
                                        case "rbCTCSend":
                                            {
                                                sDelivery = "C";
                                                break;
                                            }
                                        case "rbFTPSend":
                                            {
                                                sDelivery = "F";
                                                break;
                                            }
                                        case "rbFTPSendDirect":
                                            {
                                                sDelivery = "D";
                                                break;
                                            }
                                        case "rbEmailSend":
                                            {
                                                sDelivery = "E";
                                                break;
                                            }
                                        case "rbSoapSend":
                                            {
                                                sDelivery = "S";
                                                break;
                                            }
                                    }
                                }
                            }
                            profile.P_DELIVERY = sDelivery;
                        }
                        if (rbReceive.Checked)
                        {
                            profile.P_PORT = edFTPReceivePort.Text;
                            profile.P_SERVER = edServerAddress.Text;
                            profile.P_USERNAME = edReceiveUsername.Text;
                            profile.P_PASSWORD = edReceivePassword.Text;
                            profile.P_SUBJECT = edReceiveSubject.Text;
                            profile.P_PATH = edFTPReceivePath.Text;
                            profile.P_EMAILADDRESS = edReceiveEmailAddress.Text;
                            profile.P_CUSTOMERCOMPANYNAME = edReceiveCustomerName.Text;
                            profile.P_SENDEREMAIL = edReceiveSender.Text;
                            string sDelivery = string.Empty;
                            foreach (RadioButton rb in gbReceiveMethod.Controls)
                            {
                                if (rb.Checked)
                                {
                                    switch (rb.Name)
                                    {

                                        case "rbPickup":
                                            {
                                                sDelivery = "R";

                                                break;
                                            }
                                        case "rbSendCTC":
                                            {
                                                sDelivery = "C";
                                                break;
                                            }
                                        case "rbSendFTP":
                                            {
                                                sDelivery = "F";
                                                break;
                                            }
                                        case "rbEmail":
                                            {
                                                sDelivery = "E";
                                                break;
                                            }
                                        case "rbReceivePickup":
                                            {
                                                sDelivery = "P";
                                                break;
                                            }
                                    }
                                }
                            }
                            profile.P_DELIVERY = sDelivery;
                        }
                        db.Profiles.Add(profile);

                    }
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Saving Record : " + ex.Message);
                }
            }
            FillGrid(Globals.Customer.Id);
            if (rbPickup.Checked)
            {
                //CreateProfPath(Path.Combine(edRootPath.Text + "\\Processing"), edRecipientID.Text);
            }

            edDescription.Text = "";
            edReasonCode.Text = "";
            edRecipientID.Text = "";
            rbPickup.Checked = true;
            edFTPReceivePort.Text = "";
            edServerAddress.Text = "";
            edReceivePassword.Text = "";
            edReceivePassword.Text = "";
            btnAdd.Text = "&Add  >>";

        }

        private string PutXSD(string fileName)
        {

            bool filecopied = false;
            string file = Path.GetFileNameWithoutExtension(fileName);
            int i = 1;
            while (!filecopied)
            {
                try
                {
                    {
                        if (!File.Exists(Path.Combine(Globals.LibPath, file + Path.GetExtension(fileName))))
                        {
                            File.Copy(fileName, Path.Combine(Globals.LibPath, file + Path.GetExtension(fileName)));
                            filecopied = true;
                        }
                        else
                        {
                            filecopied = true;
                        }
                    }
                }
                catch (IOException)
                {
                    i++;
                    fileName = file + i + Path.GetExtension(fileName);

                }
            }
            return file + Path.GetExtension(fileName);
        }

        private void btnAddParam_Click(object sender, EventArgs e)
        {
            lbParameters.Items.Add(edParameter.Text);
        }

        private void dgProfile_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }

            bool isActive = true;

            foreach (DataGridViewRow row in dgv.SelectedRows)
            {
                if ((string)row.Cells["Active"].Value == "Y")
                {
                    isActive = true;
                }

                if ((string)row.Cells["Active"].Value == "N")
                {
                    isActive = false;
                }
            }
            if (isActive)
            {
                markInactiveToolStripMenuItem.Text = "Mark InActive";
            }
            else
            {
                markInactiveToolStripMenuItem.Text = "Mark Active";
            }
        }

        public void GetCustomerProfile(int dgRow)
        {

            if (Guid.TryParse(dgProfile[0, dgRow].Value.ToString(), out gP_id))
            {
                using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
                {
                    var profile = (from p in db.Profiles
                                   where p.P_ID == gP_id
                                   select p).FirstOrDefault();
                    if (profile != null)
                    {
                        edDescription.Text = profile.P_DESCRIPTION;
                        edReasonCode.Text = profile.P_REASONCODE;
                        edEventCode.Text = profile.P_EVENTCODE;
                        edRecipientID.Text = profile.P_RECIPIENTID;
                        edSenderID.Text = profile.P_SENDERID;
                        cbNotify.Checked = profile.P_NOTIFY == "Y" ? true : false;
                        edNotifyEmail.Text = profile.P_NOTIFYEMAIL;
                        //TODO Repair XSD Load. 
                        //rtbXSD.Text = profile.P_XSD;
                        edXSDPath.Text = profile.P_XSD;

                        edLibraryName.Text = profile.P_LIBNAME;
                        edCustomMethod.Text = profile.P_METHOD;
                        edRecipientID.Text = profile.P_RECIPIENTID;
                        cmbMsgType.Text = profile.P_MSGTYPE;
                        cbProfileActive.Checked = profile.P_ACTIVE == "Y" ? true : false;
                        cbGroupCharges.Checked = profile.P_GROUPCHARGES == "Y" ? true : false;
                        cbDTS.Checked = profile.P_DTS == "N" ? false : true;
                        if (profile.P_DIRECTION == "R")
                        {
                            gbReceiveOptions.Enabled = true;
                            gbSendOptions.Enabled = false;
                            rbReceive.Checked = true;
                            rbSend.Checked = false;
                            edServerAddress.Text = profile.P_SERVER;
                            edFTPReceivePort.Text = profile.P_PORT;
                            edFTPReceivePath.Text = profile.P_PATH;
                            edReceiveUsername.Text = profile.P_USERNAME;
                            edReceivePassword.Text = profile.P_PASSWORD;
                            edReceiveCustomerName.Text = profile.P_CUSTOMERCOMPANYNAME;
                            edReceiveEmailAddress.Text = profile.P_EMAILADDRESS;
                            cbSSL.Checked = profile.P_SSL == "Y" ? true : false;
                            edReceiveSubject.Text = profile.P_SUBJECT;
                            edReceiveSender.Text = profile.P_SENDEREMAIL;
                            cmbFileType.Text = profile.P_FILETYPE;
                            switch (profile.P_DELIVERY)
                            {
                                case "R":
                                    {
                                        rbPickup.Checked = true;
                                        break;
                                    }
                                case "F":
                                    {
                                        rbReceiveFTP.Checked = true;
                                        break;
                                    }
                                case "E":
                                    {
                                        rbEmail.Checked = true;
                                        break;
                                    }
                                case "P":
                                    {
                                        rbReceivePickup.Checked = true;
                                        break;
                                    }
                                default:
                                    {
                                        rbPickup.Checked = true;
                                        break;
                                    }
                            }
                            SetupReceive();
                        }
                        else
                        {
                            gbReceiveOptions.Enabled = false;
                            gbSendOptions.Enabled = true;
                            rbReceive.Checked = false;
                            rbSend.Checked = true;
                            edSendCTCUrl.Text = profile.P_SERVER;
                            edSendFolder.Text = profile.P_PATH;
                            edSendFtpPort.Text = profile.P_PORT;
                            edSendUsername.Text = profile.P_USERNAME;
                            edSendPassword.Text = profile.P_PASSWORD;
                            edSendSubject.Text = profile.P_SUBJECT;
                            edSendEmailAddress.Text = profile.P_EMAILADDRESS;
                            switch (profile.P_DELIVERY)
                            {
                                case "R":
                                    {
                                        rbPickupSend.Checked = true;
                                        break;
                                    }
                                case "F":
                                    {
                                        rbFTPSend.Checked = true;
                                        break;
                                    }
                                case "D":
                                    {
                                        rbFTPSendDirect.Checked = true;
                                        break;
                                    }
                                case "E":
                                    {
                                        rbEmailSend.Checked = true;
                                        break;
                                    }
                                case "C":
                                    {
                                        rbCTCSend.Checked = true;
                                        break;
                                    }
                                default:
                                    {
                                        rbPickupSend.Checked = true;
                                        break;
                                    }
                            }
                            SetupSend();
                        }
                        btnAdd.Text = "&Update >>";
                        FillDTSGrid(gP_id);
                    }
                }
            }
        }


        private void dgProfile_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GetCustomerProfile(e.RowIndex);
        }

        private void dgProfile_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "R")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LawnGreen;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "S")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSkyBlue;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Active"].Value.ToString() == "N")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.LightGray;
            }
        }

        private void dgProfile_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DataGridViewCell c = (sender as DataGridView)[e.ColumnIndex, e.RowIndex];
                if (!c.Selected)
                {
                    c.DataGridView.ClearSelection();
                    c.DataGridView.CurrentCell = c;
                    c.Selected = true;
                }
            }
        }

        private void markInactiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            //  DataGridViewRow row = dgProfile.Rows[rowIndex];
            String strActiveToggle = "";
            if (markInactiveToolStripMenuItem.Text == "Mark InActive")
            {
                strActiveToggle = "N";
            }

            if (markInactiveToolStripMenuItem.Text == "Mark Active")
            {
                strActiveToggle = "Y";
            }

            foreach (DataGridViewRow row in dgProfile.SelectedRows)
            {
                using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
                {
                    Guid p_id = new Guid(row.Cells[0].Value.ToString());
                    var prof = (from p in db.Profiles
                                where p.P_ID == p_id
                                select p).FirstOrDefault();
                    if (prof != null)
                    {
                        prof.P_ACTIVE = strActiveToggle;
                        db.SaveChanges();
                    }

                }
            }
            FillGrid(Globals.Customer.Id);
        }

        private void duplicateProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var prof = (from p in db.Profiles
                            where p.P_ID == p_id
                            select p).FirstOrDefault();
                if (prof != null)
                {
                    Profile NewProfile = prof;
                    NewProfile.P_DESCRIPTION = prof.P_DESCRIPTION + " - Duplicate";
                    // NewProfile.P_ID = Guid.NewGuid();
                    db.Profiles.Add(NewProfile);
                    db.SaveChanges();
                }
            }
            FillGrid(Globals.Customer.Id);

        }

        private void deleteProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete this rule?", "Delete from Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {
                Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
                DeleteProfile(p_id);
            }
        }

        private void DeleteProfile(Guid pid)
        {
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var profile = (from p in db.Profiles
                               where p.P_ID == pid
                               select p).FirstOrDefault();
                if (profile != null)
                {
                    db.Profiles.Remove(profile);
                    db.SaveChanges();
                }
            }
            FillGrid(Globals.Customer.Id);
            MessageBox.Show("Client rule deleted", "Customer profile updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void dgProfile_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete this rule?", "Delete from Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {
                Guid p_id = new Guid(dgProfile[0, e.Row.Index].Value.ToString());
                DeleteProfile(p_id);
            }
        }

        private void gbDirection_Enter(object sender, EventArgs e)
        {

        }

        private void rbReceive_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void cmProfiles_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void cbHideInactive_CheckedChanged(object sender, EventArgs e)
        {
            FillGrid(Globals.Customer.Id);
        }
    }
}
