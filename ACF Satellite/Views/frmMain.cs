﻿using ACF_Satellite.Resources;
using ACF_Satellite.Services;
using ACF_Satellite.Views;
using CTCLogging;
using NodeData;
using NodeResources;
using NodeResources.Interfaces;
using SatelliteData.Models;
using SatelliteData.UnitOfWork;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using XMLLocker;
using static ACF_Satellite.SatelliteErrors;

namespace ACF_Satellite
{

    public partial class frmMain : Form
    {
        public int totfilesRx { get; set; }
        public int totfilesTx;
        public int cwRx;
        public int cwTx { get; set; }
        public int filesRx;
        public int filesTx;
        public System.Timers.Timer tmrMain;

        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer();

            this.Text = string.Format("CTC Satellite -  {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());

        }

        public static void AddLabel(Label lb, string value)
        {
            if (lb.InvokeRequired)
            {
                lb.BeginInvoke(new System.Action(delegate { AddLabel(lb, value); }));
                return;
            }
            lb.Text = value;
        }
        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                GetFiles();
                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }
        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = System.Drawing.Color.LightCoral;
                //        UpdateSBar("Timed Processes Started");    
                GetFiles();
                tmrMain.Start();

            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = System.Drawing.Color.LightGreen;
                //           UpdateSBar("Timed Processes Stopped");
                tmrMain.Stop();
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
            LoadSettings();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadSettings();
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.CTCNodeConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat(Globals.CustCode, "Starting", null);
            }
            tslMain.Width = this.Width / 2;
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);
            string appLog = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-Log.XML";
            Globals.AppLogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
               System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, appLog);
            using (AppLogger log = new AppLogger(Globals.AppLogPath, string.Empty, "Starting Application", "Starting", DateTime.Now, string.Empty, string.Empty))
            {
                log.AddLog();
            }
        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);


            }
            tmrMain.Interval = 60 * (1000 * 5);

            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.AppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.AppConfig = Path.Combine(path, Globals.AppConfig);
            frmSettings settings = new frmSettings();
            bool loadSettings = false;
            if (!File.Exists(Globals.AppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.AppConfig);
                settings.ShowDialog();
            }
            else
            {
                try
                {
                    XDocument doc = XDocument.Load(Globals.AppConfig);

                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.AppConfig);


                    var node = doc.Root.Element("Customer");
                    if (node == null)
                    {
                        loadSettings = true;
                    }
                    else
                    {
                        Globals.CustCode = node.Element("Code") == null ? string.Empty : node.Element("Code").Value;
                        Globals.PrimaryLocation = node.Element("ProfilePath") == null ? "" : node.Element("ProfilePath").Value;
                        Globals.ArchiveLocation = node.Element("ArchiveLocation") == null ? "" : node.Element("ArchiveLocation").Value;
                        Globals.TestPrimaryLocation = node.Element("TestPath") == null ? "" : node.Element("TestPath").Value;
                        Globals.PickupLocation = node.Element("PickupPath") == null ? "" : node.Element("PickupPath").Value;
                        Globals.TestPickupLocation = node.Element("TestPath") == null ? "" : node.Element("TestPath").Value;
                        Globals.FailPath = node.Element("FailPath") == null ? "" : node.Element("FailPath").Value;
                        Globals.CustomFilesPath = node.Element("CustomFilesPath") == null ? "" : node.Element("CustomFilesPath").Value;
                        Globals.OutputLocation = node.Element("OutputPath") == null ? "" : node.Element("OutputPath").Value;
                        Globals.LibPath = node.Element("LibLocation") == null ? "" : node.Element("LibLocation").Value;
                    }

                    node = doc.Root.Element("Database");

                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        loadSettings = true;
                    }
                    else
                    {
                        Globals.ConnString = new NodeData.ConnectionManager(
                            node.Element("Servername").Value,
                            node.Element("Catalog").Value,
                            node.Element("Username").Value,
                            node.Element("Password").Value);
                        Globals.GetCustomer(Globals.CustCode, Globals.ConnString.ConnString);
                    }

                    node = doc.Root.Element("CTCNodeDatabase");
                    if (node == null)
                    {
                        loadSettings = true;
                    }
                    else
                    {
                        Globals.CTCNodeConnString = new NodeData.ConnectionManager(
                                               node.Element("Servername").Value,
                                               node.Element("Catalog").Value,
                                               node.Element("Username").Value,
                                               node.Element("Password").Value);
                    }


                    node = doc.Root.Element("Communications");
                    if (node == null)
                    {
                        loadSettings = true;
                    }
                    else
                    {
                        Globals.MailServerSettings = new MailServerSettings
                        {
                            Server = node.Element("SMTPServer").Value,
                            UserName = node.Element("SMTPUsername").Value,
                            Password = node.Element("SMTPPassword").Value,
                            IsSecure = node.Element("SMTPSsl") == null ? false : bool.Parse(node.Element("SMTPSsl").Value),
                            Port = int.Parse(node.Element("SMTPServer").Attribute("Port").Value),
                            Email = node.Element("EmailAddress").Value
                        };
                        Globals.AlertsTo = node.Element("AlertsTo") == null ? "" : node.Element("AlertsTo").Value;
                    }

                    if (loadSettings)
                    {
                        MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        frmSettings FormSettings = new frmSettings();
                        FormSettings.ShowDialog();
                        DialogResult dr = FormSettings.DialogResult;
                        if (dr == DialogResult.OK)
                        {
                            LoadSettings();
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    if (loadSettings)
                    {
                        MessageBox.Show("There was an error loading the Config files. Please check the settings" + Environment.NewLine +
                                                "Error " + strEx + " " + ex.Message, "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        frmSettings FormSettings = new frmSettings();
                        FormSettings.ShowDialog();
                        DialogResult dr = FormSettings.DialogResult;
                        if (dr == DialogResult.OK)
                        {
                            LoadSettings();
                        }
                    }

                }

                try
                {
                    this.Text = "CTC Satellite - " + Globals.Customer.CustomerName;
                }
                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }

                AddText(rtbLog, "System Ready", Color.Green);
            }
            //  SatelliteData.HeartBeat.RegisterHeartBeat(Globals.Customer.Code, Globals.Customer.Id, "Starting", null, Path.GetDirectoryName(Application.ExecutablePath));
        }

        public static void AddText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value); }));
                return;
            }

            rtb.AppendText(string.Format("{0:g} ", DateTime.Now) + value + Environment.NewLine);
            rtb.ScrollToCaret();

        }
        public static void AddText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(string.Format("{0:g} ", DateTime.Now) + value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetFiles();

        }

        private void GetFiles()
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.CTCNodeConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat(Globals.CustCode, m.Name, m.GetParameters());
            }
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.PickupLocation;
            }
            else
            {
                filesPath = Globals.TestPickupLocation;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            int iCount = 0;
            var stopwatch = new Stopwatch();

            var list = diTodo.GetFiles();
            if (list.Length > 0)
            {
                stopwatch.Start();
                AddText(rtbLog, " Now Processing " + list.Length + " Files");
                foreach (var fileName in diTodo.GetFiles())
                {

                    totfilesRx++;
                    AddLabel(lblTotFilesRx, totfilesRx.ToString());
                    iCount++;
                    ProcessResult thisResult = new ProcessResult();
                    thisResult = ProcessCustomFiles(fileName.FullName);
                    //return;
                    if (thisResult.Processed)
                    {
                        try
                        {
                            if (File.Exists(fileName.FullName))
                            {
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                File.Delete(fileName.FullName);
                            }
                        }
                        catch (Exception ex)
                        {
                            string strEx = ex.GetType().Name;
                            string v = string.Format("{0}.{1}", m.ReflectedType.Name, m.Name);
                            using (AppLogger log = new AppLogger(Globals.AppLogPath, string.Empty, "Error " + strEx + ": " + ex.Message, v, DateTime.Now, ex.Message, fileName.FullName))
                            {
                                log.AddLog();
                            }
                        }
                    }
                }
                stopwatch.Stop();
                double t = stopwatch.Elapsed.TotalSeconds;
                AddText(rtbLog, " Processing Complete. " + iCount + " files processed in " + t + " seconds", Color.Blue);
            }
            else
            {
                AddText(rtbLog, " Waiting for files.");
            }
        }

        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            //    NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            //    NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            //    TransReference trRef = new TransReference();
            switch (processingFile.Extension.ToUpper())
            {
                case ".XML":
                    thisResult = ProcessCargowiseFiles(xmlFile);
                    break;
                case ".CSV":
                    break;

            }

            return thisResult;
        }

        private ProcessResult ProcessCargowiseFiles(string xmlFile)
        {
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.CTCNodeConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                MethodBase m = MethodBase.GetCurrentMethod();
                heartBeat.RegisterHeartBeat(Globals.CustCode, m.Name, m.GetParameters());
            }
            FileInfo processingFile = new FileInfo(xmlFile);
            AddText(rtbLog, " File Found " + Path.GetFileName(xmlFile) + ". Checking for Cargowise File.", Color.Gray);
            var ns = XMLLocker.Cargowise.CWFunctions.GetNameSpace(xmlFile);
            String appCode = XMLLocker.Cargowise.CWFunctions.GetApplicationCode(ns.NamespaceName);

            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            ProcessResult canDelete = new ProcessResult();
            switch (appCode)
            {

                case "UDM":
                    AddText(rtbLog, " File is Cargowise (UDM) XML File :" + xmlFile + ". " + "", Color.Gray);
                    //if (NodeResources.GetXMLType(xmlfile) == "UniversalEvent")
                    //{

                    //}
                    //else
                    //{
                    ////    canDelete = ProcessUDM(xmlfile);
                    //}
                    break;
                case "NDM":
                    AddText(rtbLog, " File is Cargowise (NDM) XML File :" + xmlFile + ". " + "", Color.Gray);
                    //  canDelete = ProcessProductResponse(xmlfile);
                    break;
                default:
                    AddText(rtbLog, " File is not a valid Cargowise XML File :" + xmlFile + ". Checking Client Specific formats" + "", Color.Gray);
                    canDelete = ProcessCustomXML(xmlFile);
                    break;
            }

            if (canDelete.Processed)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                File.Delete(canDelete.FileName);
            }
            else
            {
                if (canDelete.FileName != null)
                {
                    string f = Global.MoveFile(xmlFile, Globals.FailPath);
                    if (f.StartsWith("Warning"))
                    {
                        AddText(rtbLog, f, Color.Red);
                    }
                    else
                    { xmlFile = f; }
                }
            }
            return canDelete;
        }

        private ProcessResult ProcessCustomXML(string XMLFile)
        {
            ProcessResult result = new ProcessResult();
            result.FileName = XMLFile;
            result.FolderLoc = Path.GetDirectoryName(XMLFile);
            IArcOps arc = new ArcOps();
            var archivedFile = arc.ArchiveFile(Globals.ArchiveLocation, XMLFile);
            Transaction trans = new Transaction();

            if (archivedFile.Contains("Exception"))
            {
                //TODO Throw Exception
            }
            FileProcessorService fps;
            var profID = IdentifyXML(XMLFile);
            if (profID != null)
            {
                AddText(rtbLog, "File is valid. Processing", Color.Blue);
                fps = new FileProcessorService(Globals.OutputLocation, XMLFile, (Guid)profID);
                trans = fps.ProcessFile();
                if (trans != null)
                {
                    AddText(rtbLog, "Processing " + XMLFile + " " + trans.T_LOGTYPE + ":" + trans.T_MSGTYPE, trans.T_LOGTYPE == "Success" ? Color.Green : Color.Red);
                    cwTx++;
                    AddLabel(lblCwRx, cwTx.ToString());
                }
            }
            else
            {
                XDocument doc = XDocument.Load(XMLFile);
                var ord = XMLHelper.GetNodeValue(doc, "SalesOrder", "OrderNo");
                if (ord != null)
                {
                    fps = new FileProcessorService(Globals.OutputLocation, XMLFile);
                    trans = fps.ProcessFile();
                    if (trans != null)
                    {
                        AddText(rtbLog, "Processing" + trans.T_LOGTYPE + ":" + trans.T_MSGTYPE, trans.T_LOGTYPE == "Success" ? Color.Green : Color.Red);
                    }

                }
            }
            if (trans.T_LOGTYPE.Trim() != "Success")
            {
                using (MailModule mail = new MailModule(Globals.MailServerSettings))
                {
                    var m = MethodBase.GetCurrentMethod();


                    mail.SendMsg(XMLFile, Globals.Support, trans.T_LOGTYPE + ":" + m.Name, trans.T_LOGTYPE + " Found. Processing " + XMLFile + Environment.NewLine + trans.T_LOGMSG);
                }
            }
            trans.T_ARCHIVE = archivedFile;
            using (SatelliteUnitOfWork sUoW = new SatelliteUnitOfWork(new SatelliteEntity(Globals.ConnString.ConnString)))
            {

                sUoW.Transactions.Add(TrimStrings.TrimStringProps(trans));

                var i = sUoW.Complete();
                if (i == -1)
                {
                    using (MailModule mail = new MailModule(Globals.MailServerSettings))
                    {
                        var m = MethodBase.GetCurrentMethod();
                        mail.SendMsg(XMLFile, Globals.Support, "ACF Satellite Error Found.", "Processing " + XMLFile + Environment.NewLine + sUoW.ErrorLog);
                    }
                }

            }
            result.Processed = true;
            result.FileName = XMLFile;
            return result;
        }



        private bool CreateCWFile(XMLLocker.Cargowise.XUS.UniversalInterchange cwFile, string recipient, string sender)
        {
            try
            {
                String cwXML = Path.Combine(Globals.OutputLocation, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(Globals.OutputLocation, recipient + "-" + sender + string.Format("{0:yyMMddhhmmss}", DateTime.Now) + "-" + iFileCount + ".xml");
                }
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                using (StringWriter writer = new StringWriter())
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.Cargowise.XUS.UniversalInterchange));
                    var cwNSUniversal = new XmlSerializerNamespaces();
                    cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                    xSer.Serialize(outputCW, cwFile, cwNSUniversal);
                    outputCW.Flush();
                    outputCW.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                AddText(rtbLog, "Error Found Creating Cargowise File. Check error log for details.", Color.Red);
                Global.LogError(recipient + "-" + sender, ex.GetType().Name, ex.Source, DateTime.Now, ex.ToLogString(ex.StackTrace), ex.TargetSite.Name);
                return false;
            }




        }


        private Guid? IdentifyXML(string xmlFile)
        {
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.CTCNodeConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                MethodBase m = MethodBase.GetCurrentMethod();
                heartBeat.RegisterHeartBeat(Globals.CustCode, m.Name, m.GetParameters());
            }
            using (ISatelliteUnitOfWork uow = new SatelliteUnitOfWork(new SatelliteEntity(Globals.ConnString.ConnString)))
            {
                var pList = uow.Profiles.Find(x => x.P_MSGTYPE == "Conversion" && x.P_XSD != null)
                                        .OrderBy(x => x.P_DESCRIPTION)
                                        .ToList();
                if (pList.Count > 0)

                //    using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
                //{
                //    var profList = (from p in db.Profiles
                //                    where p.P_MSGTYPE == "Conversion"
                //                    && p.P_XSD != null
                //                    orderby p.P_DESCRIPTION
                //                    select new { p.P_ID, p.P_XSD, p.P_DESCRIPTION }).ToList();
                //    if (profList.Count > 0)
                {
                    foreach (var p in pList)
                    {
                        try
                        {
                            string schemaFile = Path.Combine(Globals.LibPath, p.P_XSD);
                            XmlTextReader sXsd = new XmlTextReader(schemaFile);
                            XmlSchema schema = new XmlSchema();
                            string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                            schema = XmlSchema.Read(sXsd, CustomValidateHandler.HandlerErrors);
                            XmlReaderSettings settings = new XmlReaderSettings();
                            settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                            settings.ValidationType = ValidationType.Schema;
                            settings.Schemas.Add(schema);
                            settings.ValidationEventHandler += (o, ea) =>
                            {
                                throw new XmlSchemaValidationException(
                                    string.Format("Schema Not Found: {0}",
                                                  ea.Message),
                                    ea.Exception);
                            };
                            using (var stream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                            using (var cusXML = XmlReader.Create(stream, settings))
                            {
                                while (cusXML.Read())
                                {

                                }
                                stream.Close();

                            }
                            return p.P_ID;
                        }
                        catch (XmlSchemaValidationException ex)
                        {
                            AddText(rtbLog, "Invalid Schema found. Continue Scanning for matching Customer Schema " + ex.Message, Color.Red);
                            Global.LogError(p.P_DESCRIPTION, ex.GetType().Name, ex.Source, DateTime.Now, ex.ToLogString(ex.StackTrace), ex.TargetSite.Name);

                        }
                        catch (XmlException ex)
                        {
                            AddText(rtbLog, "CTC Node (XML) Exception Found: Custom Processing:" + ex.Message + ". " + "", Color.Red);
                            Global.LogError(p.P_DESCRIPTION, ex.GetType().Name, ex.Source, DateTime.Now, ex.ToLogString(ex.StackTrace), ex.TargetSite.Name);
                            //   return thisResult;

                        }
                        catch (Exception ex)
                        {
                            AddText(rtbLog, "CTC Node (Trap All) Exception Found: Custom Processing:" + ex.Message + ". " + "", Color.Red);
                            Global.LogError(p.P_DESCRIPTION, ex.GetType().Name, ex.Source, DateTime.Now, ex.ToLogString(ex.StackTrace), ex.TargetSite.Name);
                            //   return thisResult;
                        }
                    }
                }
            }
            return null;

        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void importProductsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConvertProduct importProds = new frmConvertProduct();
            importProds.Show();


        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.CTCNodeConnString.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat(Globals.CustCode, "Stopping", null);
            }
        }
    }
}

