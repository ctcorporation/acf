﻿namespace ACF_Satellite.Views
{
    partial class frmConvertProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConvertProduct));
            this.label11 = new System.Windows.Forms.Label();
            this.cmbMapList = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMapDescription = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSaveMapping = new System.Windows.Forms.Button();
            this.btnLoadMapping = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDataTypes = new System.Windows.Forms.ComboBox();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.txtOwner = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lvFieldList = new System.Windows.Forms.ListBox();
            this.btnAddMapping = new System.Windows.Forms.Button();
            this.btnAddField = new System.Windows.Forms.Button();
            this.btnAddRequiredField = new System.Windows.Forms.Button();
            this.dgMapping = new System.Windows.Forms.DataGridView();
            this.Col1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lvRequiredFields = new System.Windows.Forms.ListBox();
            this.cmbConversion = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.txtFileToConvert = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConvert = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnImportOnly = new System.Windows.Forms.Button();
            this.btnBulkExport = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(200, 315);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 31;
            this.label11.Text = "Data Type";
            // 
            // cmbMapList
            // 
            this.cmbMapList.FormattingEnabled = true;
            this.cmbMapList.Location = new System.Drawing.Point(126, 392);
            this.cmbMapList.Name = "cmbMapList";
            this.cmbMapList.Size = new System.Drawing.Size(453, 21);
            this.cmbMapList.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 393);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Select Saved Map";
            // 
            // txtMapDescription
            // 
            this.txtMapDescription.Location = new System.Drawing.Point(126, 366);
            this.txtMapDescription.Name = "txtMapDescription";
            this.txtMapDescription.Size = new System.Drawing.Size(453, 20);
            this.txtMapDescription.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 367);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "Map Description ";
            // 
            // btnSaveMapping
            // 
            this.btnSaveMapping.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveMapping.Image")));
            this.btnSaveMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveMapping.Location = new System.Drawing.Point(585, 364);
            this.btnSaveMapping.Name = "btnSaveMapping";
            this.btnSaveMapping.Size = new System.Drawing.Size(113, 23);
            this.btnSaveMapping.TabIndex = 26;
            this.btnSaveMapping.Text = "&Save Mapping";
            this.btnSaveMapping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveMapping.UseVisualStyleBackColor = true;
            this.btnSaveMapping.Click += new System.EventHandler(this.btnSaveMapping_Click);
            // 
            // btnLoadMapping
            // 
            this.btnLoadMapping.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadMapping.Image")));
            this.btnLoadMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoadMapping.Location = new System.Drawing.Point(585, 393);
            this.btnLoadMapping.Name = "btnLoadMapping";
            this.btnLoadMapping.Size = new System.Drawing.Size(113, 23);
            this.btnLoadMapping.TabIndex = 25;
            this.btnLoadMapping.Text = "Load Mapping";
            this.btnLoadMapping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLoadMapping.UseVisualStyleBackColor = true;
            this.btnLoadMapping.Click += new System.EventHandler(this.btnLoadMapping_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(538, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(202, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Fields (From Conversion Selection)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Select Conversion Type. ";
            // 
            // cmbDataTypes
            // 
            this.cmbDataTypes.FormattingEnabled = true;
            this.cmbDataTypes.Location = new System.Drawing.Point(268, 312);
            this.cmbDataTypes.Name = "cmbDataTypes";
            this.cmbDataTypes.Size = new System.Drawing.Size(181, 21);
            this.cmbDataTypes.TabIndex = 32;
            // 
            // txtCustomer
            // 
            this.txtCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCustomer.Location = new System.Drawing.Point(508, 19);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(100, 20);
            this.txtCustomer.TabIndex = 16;
            // 
            // txtOwner
            // 
            this.txtOwner.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOwner.Location = new System.Drawing.Point(221, 19);
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Size = new System.Drawing.Size(100, 20);
            this.txtOwner.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(200, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Mapping Definition";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "(From) Cargowise Organization Code";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbDataTypes);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.cmbMapList);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtMapDescription);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.btnSaveMapping);
            this.groupBox3.Controls.Add(this.btnLoadMapping);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.lvFieldList);
            this.groupBox3.Controls.Add(this.btnAddMapping);
            this.groupBox3.Controls.Add(this.btnAddField);
            this.groupBox3.Controls.Add(this.btnAddRequiredField);
            this.groupBox3.Controls.Add(this.dgMapping);
            this.groupBox3.Controls.Add(this.lvRequiredFields);
            this.groupBox3.Location = new System.Drawing.Point(7, 191);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(783, 423);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Field Mapping ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Available Fields (From File)";
            // 
            // lvFieldList
            // 
            this.lvFieldList.FormattingEnabled = true;
            this.lvFieldList.Location = new System.Drawing.Point(11, 41);
            this.lvFieldList.Name = "lvFieldList";
            this.lvFieldList.Size = new System.Drawing.Size(182, 264);
            this.lvFieldList.TabIndex = 21;
            this.lvFieldList.DoubleClick += new System.EventHandler(this.lvFieldList_DoubleClick);
            // 
            // btnAddMapping
            // 
            this.btnAddMapping.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMapping.Image")));
            this.btnAddMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddMapping.Location = new System.Drawing.Point(248, 282);
            this.btnAddMapping.Name = "btnAddMapping";
            this.btnAddMapping.Size = new System.Drawing.Size(237, 23);
            this.btnAddMapping.TabIndex = 20;
            this.btnAddMapping.Text = "MapField";
            this.btnAddMapping.UseVisualStyleBackColor = true;
            this.btnAddMapping.Click += new System.EventHandler(this.btnAddMapping_Click);
            // 
            // btnAddField
            // 
            this.btnAddField.Location = new System.Drawing.Point(200, 282);
            this.btnAddField.Name = "btnAddField";
            this.btnAddField.Size = new System.Drawing.Size(42, 23);
            this.btnAddField.TabIndex = 19;
            this.btnAddField.Text = ">";
            this.btnAddField.UseVisualStyleBackColor = true;
            this.btnAddField.Click += new System.EventHandler(this.btnAddField_Click);
            // 
            // btnAddRequiredField
            // 
            this.btnAddRequiredField.Location = new System.Drawing.Point(491, 282);
            this.btnAddRequiredField.Name = "btnAddRequiredField";
            this.btnAddRequiredField.Size = new System.Drawing.Size(38, 23);
            this.btnAddRequiredField.TabIndex = 18;
            this.btnAddRequiredField.Text = "<";
            this.btnAddRequiredField.UseVisualStyleBackColor = true;
            this.btnAddRequiredField.Click += new System.EventHandler(this.btnAddRequiredField_Click);
            // 
            // dgMapping
            // 
            this.dgMapping.AllowUserToAddRows = false;
            this.dgMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col1,
            this.Col2,
            this.Col3});
            this.dgMapping.Location = new System.Drawing.Point(200, 60);
            this.dgMapping.Name = "dgMapping";
            this.dgMapping.ReadOnly = true;
            this.dgMapping.RowHeadersVisible = false;
            this.dgMapping.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMapping.Size = new System.Drawing.Size(329, 216);
            this.dgMapping.TabIndex = 17;
            this.dgMapping.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgMapping_UserDeletedRow);
            this.dgMapping.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgMapping_UserDeletingRow);
            // 
            // Col1
            // 
            this.Col1.DataPropertyName = "ConvertType";
            this.Col1.HeaderText = "Type";
            this.Col1.Name = "Col1";
            this.Col1.ReadOnly = true;
            this.Col1.Width = 50;
            // 
            // Col2
            // 
            this.Col2.DataPropertyName = "MapFrom";
            this.Col2.HeaderText = "Map From";
            this.Col2.Name = "Col2";
            this.Col2.ReadOnly = true;
            this.Col2.Width = 120;
            // 
            // Col3
            // 
            this.Col3.DataPropertyName = "MapTo";
            this.Col3.HeaderText = "Map To";
            this.Col3.Name = "Col3";
            this.Col3.ReadOnly = true;
            this.Col3.Width = 120;
            // 
            // lvRequiredFields
            // 
            this.lvRequiredFields.FormattingEnabled = true;
            this.lvRequiredFields.Location = new System.Drawing.Point(541, 41);
            this.lvRequiredFields.Name = "lvRequiredFields";
            this.lvRequiredFields.Size = new System.Drawing.Size(190, 264);
            this.lvRequiredFields.TabIndex = 15;
            this.lvRequiredFields.DoubleClick += new System.EventHandler(this.lvRequiredFields_DoubleClick);
            // 
            // cmbConversion
            // 
            this.cmbConversion.FormattingEnabled = true;
            this.cmbConversion.Items.AddRange(new object[] {
            "(none selected) ",
            "Native Product (Cargowise)",
            "Organisation (Cargowise)"});
            this.cmbConversion.Location = new System.Drawing.Point(165, 49);
            this.cmbConversion.Name = "cmbConversion";
            this.cmbConversion.Size = new System.Drawing.Size(271, 21);
            this.cmbConversion.TabIndex = 14;
            this.cmbConversion.SelectedIndexChanged += new System.EventHandler(this.cmbConversion_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(337, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "(To) Cargowise System Code";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbConversion);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtCustomer);
            this.groupBox2.Controls.Add(this.txtOwner);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(778, 75);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Conversion Information";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(707, 620);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 26;
            this.button1.Text = "&Close";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(525, 44);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(25, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(525, 16);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Location = new System.Drawing.Point(136, 46);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(389, 20);
            this.txtOutputFolder.TabIndex = 6;
            // 
            // txtFileToConvert
            // 
            this.txtFileToConvert.Location = new System.Drawing.Point(136, 18);
            this.txtFileToConvert.Name = "txtFileToConvert";
            this.txtFileToConvert.Size = new System.Drawing.Size(389, 20);
            this.txtFileToConvert.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Select Output Directory";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select File to Convert";
            // 
            // btnConvert
            // 
            this.btnConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvert.Image = ((System.Drawing.Image)(resources.GetObject("btnConvert.Image")));
            this.btnConvert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConvert.Location = new System.Drawing.Point(584, 620);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(113, 23);
            this.btnConvert.TabIndex = 27;
            this.btnConvert.Text = "C&reate CW File";
            this.btnConvert.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.txtOutputFolder);
            this.groupBox1.Controls.Add(this.txtFileToConvert);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(10, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(780, 84);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Convert File Details";
            // 
            // btnImportOnly
            // 
            this.btnImportOnly.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportOnly.Image = ((System.Drawing.Image)(resources.GetObject("btnImportOnly.Image")));
            this.btnImportOnly.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImportOnly.Location = new System.Drawing.Point(489, 620);
            this.btnImportOnly.Name = "btnImportOnly";
            this.btnImportOnly.Size = new System.Drawing.Size(89, 23);
            this.btnImportOnly.TabIndex = 31;
            this.btnImportOnly.Text = "&Import Only";
            this.btnImportOnly.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImportOnly.UseVisualStyleBackColor = true;
            this.btnImportOnly.Click += new System.EventHandler(this.btnImportOnly_Click);
            // 
            // btnBulkExport
            // 
            this.btnBulkExport.Location = new System.Drawing.Point(408, 620);
            this.btnBulkExport.Name = "btnBulkExport";
            this.btnBulkExport.Size = new System.Drawing.Size(75, 23);
            this.btnBulkExport.TabIndex = 32;
            this.btnBulkExport.Text = "&Bulk Export";
            this.btnBulkExport.UseVisualStyleBackColor = true;
            this.btnBulkExport.Click += new System.EventHandler(this.btnBulkExport_Click);
            // 
            // frmConvertProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 655);
            this.Controls.Add(this.btnBulkExport);
            this.Controls.Add(this.btnImportOnly);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmConvertProduct";
            this.Text = "Import Product";
            this.Load += new System.EventHandler(this.frmConvertProductData_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbMapList;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMapDescription;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSaveMapping;
        private System.Windows.Forms.Button btnLoadMapping;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbDataTypes;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.TextBox txtOwner;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox lvFieldList;
        private System.Windows.Forms.Button btnAddMapping;
        private System.Windows.Forms.Button btnAddField;
        private System.Windows.Forms.Button btnAddRequiredField;
        private System.Windows.Forms.DataGridView dgMapping;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col3;
        private System.Windows.Forms.ListBox lvRequiredFields;
        private System.Windows.Forms.ComboBox cmbConversion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.TextBox txtFileToConvert;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnImportOnly;
        private System.Windows.Forms.Button btnBulkExport;
    }
}