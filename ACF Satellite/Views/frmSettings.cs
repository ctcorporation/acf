﻿using SatelliteData;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace ACF_Satellite
{
    public partial class frmSettings : Form
    {
        SqlConnection sqlConn, sqlCTCConn;
        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void LoadSettings()
        {

            try
            {
                string connString;
                if (Globals.CTCNodeConnString == null)
                {
                    connString = System.Configuration.ConfigurationManager.ConnectionStrings["CNodeEntity"].ConnectionString;
                }
                else
                {
                    connString = Globals.CTCNodeConnString.ConnString;
                }
                ConnectionManager cNodeConn = new ConnectionManager(connString);
                if (cNodeConn != null)
                {
                    edCTCServer.Text = cNodeConn.ServerName;
                    edCTCDatabase.Text = cNodeConn.DatabaseName;
                    edCTCUsername.Text = cNodeConn.User;
                    edCTCPassword.Text = cNodeConn.Password;
                }
                if (Globals.ConnString != null)
                {
                    edServer.Text = Globals.ConnString.ServerName;
                    edInstance.Text = Globals.ConnString.DatabaseName;
                    edUserName.Text = Globals.ConnString.User;
                    edPassword.Text = Globals.ConnString.Password;
                }
                else
                {
                    connString = System.Configuration.ConfigurationManager.ConnectionStrings["SatelliteEntity"].ConnectionString;
                    ConnectionManager satConn = new ConnectionManager(connString);
                    if (satConn != null)
                    {
                        edServer.Text = satConn.ServerName;
                        edInstance.Text = satConn.DatabaseName;
                        edUserName.Text = satConn.User;
                        edPassword.Text = satConn.Password;
                    }
                }
                edMailServer.Text = Globals.MailServerSettings.Server;
                edPort.Text = Globals.MailServerSettings.Port.ToString();
                edMailUsername.Text = Globals.MailServerSettings.UserName;
                edMailPassword.Text = Globals.MailServerSettings.Password;
                edEmailAddress.Text = Globals.MailServerSettings.Email;

                edAlertsTo.Text = Globals.AlertsTo;

                edCustomerLocation.Text = Globals.PrimaryLocation;
                edCustomPath.Text = Globals.CustomFilesPath;
                edArchiveLocation.Text = Globals.ArchiveLocation;
                if (string.IsNullOrEmpty(Globals.FailPath))
                {
                    edFailPath.Text = Path.Combine(Globals.CustomFilesPath, "Failed");
                }
                else
                {
                    edFailPath.Text = Globals.FailPath;
                }
                edLibraryPath.Text = Globals.LibPath;
                edPickupLocation.Text = Globals.PickupLocation;
                edTestLocation.Text = Globals.TestPrimaryLocation;
                edOutputLocation.Text = Globals.OutputLocation;

                try
                {
                    if (Globals.Customer == null)
                    {
                        CreateNewCustomer();
                        LoadSettings();
                        return;
                    }
                    else
                    {
                        edCustomerCode.Text = Globals.Customer.Code;
                        lblCustomerName.Text = Globals.Customer.CustomerName;
                        edShortName.Text = Globals.Customer.ShortName;
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("There was an error connecting to the Database.\n Please check the Database settings \n Error was: " + ex.Message, "SQL Database Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tcSettings.SelectedIndex = 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading Settings. Error was: \n" + ex.Message, "Error Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            CheckLocations();
            XmlDocument xmlConfig = new XmlDocument();
            xmlConfig.Load(Globals.AppConfig);
            //Configuration Section Database

            XmlNodeList nodelist = xmlConfig.SelectNodes("/Satellite/Database");
            XmlNode xmlCatalog = nodelist[0].SelectSingleNode("Catalog");
            XmlNode xnode;
            if (xmlCatalog == null)
            {
                xnode = xmlConfig.CreateElement("Catalog");
                xnode.InnerText = edInstance.Text;
                if (Globals.ConnString == null)
                {
                    Globals.ConnString = new NodeData.ConnectionManager();
                }
                Globals.ConnString.DatabaseName = edInstance.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.ConnString.DatabaseName = edInstance.Text;
                xmlCatalog.InnerText = edInstance.Text;
            }
            XmlNode xmlServer = nodelist[0].SelectSingleNode("Servername");
            if (xmlServer == null)
            {
                xnode = xmlConfig.CreateElement("Servername");
                xnode.InnerText = edServer.Text;
                Globals.ConnString.ServerName = edServer.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlServer.InnerText = edServer.Text;
                Globals.ConnString.ServerName = edServer.Text;
            }


            XmlNode xmlUser = nodelist[0].SelectSingleNode("Username");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Username");
                xnode.InnerText = edUserName.Text;
                Globals.ConnString.User = edUserName.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlUser.InnerText = edUserName.Text;
                Globals.ConnString.User = edUserName.Text;
            }


            XmlNode xmlPassword = nodelist[0].SelectSingleNode("Password");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Password");
                xnode.InnerText = edPassword.Text;
                Globals.ConnString.Password = edPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.ConnString.Password = edPassword.Text;
                xmlPassword.InnerText = edPassword.Text;
            }

            if (Globals.CTCNodeConnString == null)
            {
                Globals.CTCNodeConnString = new NodeData.ConnectionManager(
                    edCTCServer.Text,
                    edCTCDatabase.Text,
                    edCTCUsername.Text,
                    edCTCPassword.Text);
            }
            nodelist = xmlConfig.SelectNodes("/Satellite/CTCNodeDatabase");
            if (nodelist.Count == 0)
            {
                XmlNode xRoot = xmlConfig.SelectSingleNode("/Satellite");
                xnode = xmlConfig.CreateElement("CTCNodeDatabase");
                xRoot.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/Satellite/CTCNodeDatabase");
            }
            xmlCatalog = null;
            xmlCatalog = nodelist[0].SelectSingleNode("Catalog");

            xnode = null;
            if (xmlCatalog == null)
            {
                xnode = xmlConfig.CreateElement("Catalog");
                xnode.InnerText = edCTCDatabase.Text;
                Globals.CTCNodeConnString.DatabaseName = edCTCDatabase.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.CTCNodeConnString.DatabaseName = edCTCDatabase.Text;
                xmlCatalog.InnerText = edCTCDatabase.Text;
            }
            xmlServer = null;
            xmlServer = nodelist[0].SelectSingleNode("Servername");
            if (xmlServer == null)
            {
                xnode = xmlConfig.CreateElement("Servername");
                xnode.InnerText = edCTCServer.Text;
                Globals.CTCNodeConnString.ServerName = edCTCServer.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlServer.InnerText = edCTCServer.Text;
                Globals.CTCNodeConnString.ServerName = edCTCServer.Text;
            }

            xmlUser = null;

            xmlUser = nodelist[0].SelectSingleNode("Username");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Username");
                xnode.InnerText = edCTCUsername.Text;
                Globals.CTCNodeConnString.User = edCTCUsername.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlUser.InnerText = edCTCUsername.Text;
                Globals.CTCNodeConnString.User = edCTCUsername.Text;
            }

            xmlPassword = null;
            xmlPassword = nodelist[0].SelectSingleNode("Password");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Password");
                xnode.InnerText = edCTCPassword.Text;
                Globals.CTCNodeConnString.Password = edCTCPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.CTCNodeConnString.Password = edCTCPassword.Text;
                xmlPassword.InnerText = edCTCPassword.Text;
            }

            nodelist = null;

            //configuration Section Communications
            nodelist = xmlConfig.SelectNodes("/Satellite/Communications");
            XmlNode xmlAlerts = nodelist[0].SelectSingleNode("AlertsTo");
            if (xmlAlerts == null)
            {
                xnode = xmlConfig.CreateElement("AlertsTo");
                xnode.InnerText = edAlertsTo.Text;
                Globals.AlertsTo = edAlertsTo.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.AlertsTo = edAlertsTo.Text;
                xmlAlerts.InnerText = edAlertsTo.Text;
            }

            XmlNode xmlSMTP = nodelist[0].SelectSingleNode("SMTPServer");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPServer");
                xnode.InnerText = edMailServer.Text;
                XmlAttribute xPort = xmlConfig.CreateAttribute("Port");
                xPort.InnerXml = edPort.Text;
                if (Globals.MailServerSettings == null)
                {
                    Globals.MailServerSettings = new NodeResources.MailServerSettings();
                }
                Globals.MailServerSettings.Port = int.Parse(edPort.Text);
                xnode.Attributes.Append(xPort);
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edMailServer.Text;
                Globals.MailServerSettings.Server = edMailServer.Text;
                xmlSMTP.Attributes[0].InnerXml = edPort.Text;
                Globals.MailServerSettings.Port = int.Parse(edPort.Text);
            }
            xmlSMTP = nodelist[0].SelectSingleNode("EmailAddress");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("EmailAddress");
                xnode.InnerText = edEmailAddress.Text;
                Globals.MailServerSettings.Email = edEmailAddress.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edEmailAddress.Text;
                Globals.MailServerSettings.Email = edEmailAddress.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("SMTPUsername");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPUsername");
                xnode.InnerText = edMailUsername.Text;
                Globals.MailServerSettings.UserName = edMailUsername.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edMailUsername.Text;
                Globals.MailServerSettings.UserName = edMailUsername.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("SMTPPassword");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPPassword");
                xnode.InnerText = edMailPassword.Text;
                Globals.MailServerSettings.Password = edMailPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edMailPassword.Text;
                Globals.MailServerSettings.Password = edMailPassword.Text;
            }

            //Configuration Section Customer

            nodelist = xmlConfig.SelectNodes("/Satellite/Customer");
            if (nodelist[0] == null)
            {
                xnode = xmlConfig.CreateElement("Customer");
                XmlNode xConfig = xmlConfig.SelectSingleNode("/Satellite");
                xConfig.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/Satellite/Customer");
            }

            XmlNode xmlCustomer = nodelist[0].SelectSingleNode("Name");
            if (xmlCustomer == null)
            {
                xnode = xmlConfig.CreateElement("Name");
                xnode.InnerText = lblCustomerName.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlCustomer.InnerText = lblCustomerName.Text;
            }

            XmlNode xmlCustCode = nodelist[0].SelectSingleNode("Code");
            if (xmlCustCode == null)
            {
                xnode = xmlConfig.CreateElement("Code");
                xnode.InnerText = edCustomerCode.Text;
                if (Globals.Customer == null)
                {
                    CreateNewCustomer();
                    Globals.Customer = new Customer();
                }
                Globals.Customer.Code = edCustomerCode.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlCustCode.InnerText = edCustomerCode.Text;
                Globals.Customer.Code = edCustomerCode.Text;
            }

            XmlNode xmlTestPath = nodelist[0].SelectSingleNode("TestPath");
            if (xmlTestPath == null)
            {
                xnode = xmlConfig.CreateElement("TestPath");
                xnode.InnerText = edTestLocation.Text;
                Globals.TestPrimaryLocation = edTestLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlTestPath.InnerText = edTestLocation.Text;
                Globals.TestPrimaryLocation = edTestLocation.Text;
            }

            XmlNode xmlXMLLoc = nodelist[0].SelectSingleNode("ProfilePath");
            if (xmlXMLLoc == null)
            {
                xnode = xmlConfig.CreateElement("ProfilePath");
                xnode.InnerText = Path.Combine(edCustomerLocation.Text, edCustomerCode.Text);
                Globals.PrimaryLocation = Path.Combine(edCustomerLocation.Text, edCustomerCode.Text);
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.PrimaryLocation = edCustomerLocation.Text;
                xmlXMLLoc.InnerText = edCustomerLocation.Text;
            }

            XmlNode xmlCustomPathLoc = nodelist[0].SelectSingleNode("CustomFilesPath");
            if (xmlCustomPathLoc == null)
            {
                xnode = xmlConfig.CreateElement("CustomFilesPath");
                xnode.InnerText = edCustomPath.Text;
                Globals.CustomFilesPath = edCustomPath.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.CustomFilesPath = edCustomPath.Text;
                xmlCustomPathLoc.InnerText = edCustomPath.Text;
            }


            XmlNode xmlReportLoc = nodelist[0].SelectSingleNode("OutputPath");
            if (xmlReportLoc == null)
            {
                xnode = xmlConfig.CreateElement("OutputPath");
                xnode.InnerText = edOutputLocation.Text;
                Globals.OutputLocation = edOutputLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlReportLoc.InnerText = edOutputLocation.Text;
                Globals.OutputLocation = edOutputLocation.Text;
            }


            XmlNode xmlRootPath = nodelist[0].SelectSingleNode("PickupPath");
            if (xmlRootPath == null)
            {
                xnode = xmlConfig.CreateElement("PickupPath");
                xnode.InnerText = edPickupLocation.Text;
                Globals.PickupLocation = edPickupLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.PickupLocation = edPickupLocation.Text;
                xmlRootPath.InnerText = edPickupLocation.Text;
            }


            //XmlNode xmlLibPath = nodelist[0].SelectSingleNode("LibraryPath");
            //if (xmlLibPath == null)
            //{
            //    xnode = xmlConfig.CreateElement("LibraryPath");
            //    xnode.InnerText = edLibraryPath.Text;
            //    Globals.glLibPath = edLibraryPath.Text;
            //    nodelist[0].AppendChild(xnode);
            //}
            //else
            //{
            //    xmlLibPath.InnerText = edLibraryPath.Text;
            //    Globals.glLibPath = edLibraryPath.Text;
            //}


            XmlNode xmlFailPath = nodelist[0].SelectSingleNode("FailPath");
            if (xmlFailPath == null)
            {
                xnode = xmlConfig.CreateElement("FailPath");
                xnode.InnerText = edFailPath.Text;
                nodelist[0].AppendChild(xnode);
                Globals.FailPath = edFailPath.Text;
            }
            else
            {
                xmlFailPath.InnerText = edFailPath.Text;
                Globals.FailPath = edFailPath.Text;
            }

            XmlNode xmlLibPath = nodelist[0].SelectSingleNode("LibLocation");
            if (xmlLibPath == null)
            {
                xnode = xmlConfig.CreateElement("LibLocation");
                xnode.InnerText = edLibraryPath.Text;
                nodelist[0].AppendChild(xnode);
                Globals.LibPath = edLibraryPath.Text;
                ;
            }
            else
            {
                xmlLibPath.InnerText = edLibraryPath.Text;
                Globals.LibPath = edLibraryPath.Text;
            }

            XmlNode xmlArchive = nodelist[0].SelectSingleNode("ArchiveLocation");
            if (xmlArchive == null)
            {
                xnode = xmlConfig.CreateElement("ArchiveLocation");
                xnode.InnerText = edArchiveLocation.Text;
                nodelist[0].AppendChild(xnode);
                Globals.ArchiveLocation = edArchiveLocation.Text;
            }
            else
            {
                Globals.ArchiveLocation = edArchiveLocation.Text;
                xmlArchive.InnerText = edArchiveLocation.Text;
            }

            xmlConfig.Save(Globals.AppConfig);
            MessageBox.Show("CTC Node Settings saved", "CTC Node Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
            LoadSettings();
        }

        private void CreateNewCustomer()
        {
            MessageBox.Show("No Customer details record. Please enter Customer Location and Customer Code first.");
            frmNewCustomer NewCustomer = new frmNewCustomer();
            NewCustomer.CustCode = edCustomerCode.Text;
            NewCustomer.CustPath = edCustomerLocation.Text;
            NewCustomer.CustShortName = edShortName.Text;
            DialogResult dr = new DialogResult();
            dr = NewCustomer.ShowDialog();
            //if (dr == DialogResult.OK) ;

        }

        private void btnLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edCustomerLocation.Text;
            fd.ShowDialog();
            edCustomerLocation.Text = fd.SelectedPath;
        }

        private void bbArcLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edArchiveLocation.Text;
            fd.ShowDialog();
            edArchiveLocation.Text = fd.SelectedPath;
        }

        private void bbLibLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edLibraryPath.Text;
            fd.ShowDialog();
            edLibraryPath.Text = fd.SelectedPath;
        }

        private void bbPickLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edPickupLocation.Text;
            fd.ShowDialog();
            edPickupLocation.Text = fd.SelectedPath;
        }

        private void bbOutLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edOutputLocation.Text;
            fd.ShowDialog();
            edOutputLocation.Text = fd.SelectedPath;
        }

        private void btnCheckDB_Click(object sender, EventArgs e)
        {
            rtbDBCheck.Clear();
            rtbDBCheck.Text = "Data Source=" + edServer.Text + ";Initial Catalog=" + edInstance.Text + ";User ID = " + edUserName.Text + ";Password=" + edPassword.Text + ";" + Environment.NewLine;
            rtbDBCheck.Text += "Testing Connection" + Environment.NewLine;
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = "Data Source=" + edServer.Text + ";Initial Catalog=" + edInstance.Text + ";User ID = " + edUserName.Text + ";Password=" + edPassword.Text + ";";
            try
            {
                sqlconn.Open();
                rtbDBCheck.Text += " Connected Successfully." + Environment.NewLine;
            }
            catch (SqlException ex)
            {
                rtbDBCheck.Text += "Error found :" + ex.Message + Environment.NewLine;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bbFailLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edFailPath.Text;
            fd.ShowDialog();
            edFailPath.Text = fd.SelectedPath;
        }

        private void bbCustomPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edCustomPath.Text;
            fd.ShowDialog();
            edCustomPath.Text = fd.SelectedPath;
        }

        private void bbTestPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edTestLocation.Text;
            fd.ShowDialog();
            edTestLocation.Text = fd.SelectedPath;
        }

        private void tabFilelocations_Click(object sender, EventArgs e)
        {
            CheckLocations();
        }

        private void CheckLocations()
        {
            string loc = string.Empty;
            if (edCustomerLocation.Text.Contains(edCustomerCode.Text))
            {
                loc = edCustomerLocation.Text;
            }
            else
            {
                loc = Path.Combine(edCustomerLocation.Text, edCustomerCode.Text);
            }
            Global.CreateProfPath(loc);

            if (string.IsNullOrEmpty(edArchiveLocation.Text))
            {
                if (!string.IsNullOrEmpty(edCustomerCode.Text) && !string.IsNullOrEmpty(edCustomerLocation.Text))
                {
                    edArchiveLocation.Text = Path.Combine(loc, "Archive");
                    edCustomPath.Text = Path.Combine(loc, "Custom");
                    edFailPath.Text = Path.Combine(loc, "Failed");
                    edPickupLocation.Text = Path.Combine(loc, "Processing");
                    edOutputLocation.Text = Path.Combine(loc, "Output");
                    edLibraryPath.Text = Path.Combine(loc, "Lib");
                    edTestLocation.Text = Path.Combine(loc, "Test");
                }
            }
            Global.CreateRootPath(loc);
        }



        private void btnCTCTest_Click(object sender, EventArgs e)
        {
            rtbCTCTest.Clear();
            rtbCTCTest.Text = "Data Source=" + edCTCServer.Text + ";Initial Catalog=" + edCTCDatabase.Text + ";User ID = " + edCTCUsername.Text + ";Password=" + edCTCPassword.Text + ";" + Environment.NewLine;
            rtbCTCTest.Text += "Testing Connection" + Environment.NewLine;
            SqlConnection sqlCTCConn = new SqlConnection();
            sqlCTCConn.ConnectionString = "Data Source=" + edCTCServer.Text + ";Initial Catalog=" + edCTCDatabase.Text + ";User ID = " + edCTCUsername.Text + ";Password=" + edCTCPassword.Text + ";";
            try
            {
                sqlCTCConn.Open();
                rtbCTCTest.Text += " Connected Successfully." + Environment.NewLine;
                sqlCTCConn.Close();

            }
            catch (SqlException ex)
            {
                rtbCTCTest.Text += "Error found :" + ex.Message + Environment.NewLine;
            }
        }
    }
}
