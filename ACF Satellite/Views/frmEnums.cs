﻿using SatelliteData.Models;
using System;
using System.Linq;

using System.Windows.Forms;

namespace ACF_Satellite
{
    public partial class frmEnums : Form
    {
        public Guid cw_ID;
        public frmEnums()
        {
            InitializeComponent();
        }

        private void frmEnums_Load(object sender, EventArgs e)
        {

            LoadData();
        }


        private void LoadData()
        {
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var enums = (from e in db.Cargowise_Enums
                             orderby e.CW_ENUMTYPE
                             select e).ToList();
                if (enums.Count > 0)
                {
                    dgEnums.DataSource = enums;

                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(edCargowiseValue.Text) && !string.IsNullOrEmpty(edCustomValue.Text) && !string.IsNullOrEmpty(edDatatype.Text))
            {
                using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
                {
                    var cwEnum = (from c in db.Cargowise_Enums
                                  where c.CW_ENUM == edCargowiseValue.Text
                                  select c).FirstOrDefault();
                    if (cwEnum != null)
                    {
                        cwEnum.CW_ENUMTYPE = edDatatype.Text;
                        cwEnum.CW_MAPVALUE = edCustomValue.Text;
                    }
                    else
                    {
                        cwEnum = new Cargowise_Enums
                        {
                            CW_ENUM = edCargowiseValue.Text,
                            CW_ENUMTYPE = edDatatype.Text,
                            CW_MAPVALUE = edCustomValue.Text
                        };
                        db.Cargowise_Enums.Add(cwEnum);
                    }
                    db.SaveChanges();
                }
                LoadData();
            }

        }

        private void dgEnums_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            cw_ID = (Guid)dgEnums[0, e.RowIndex].Value;
            edCargowiseValue.Text = dgEnums["CWValue", e.RowIndex].Value.ToString();
            edCustomValue.Text = dgEnums["Custom", e.RowIndex].Value.ToString();
            edDatatype.Text = dgEnums["Type", e.RowIndex].Value.ToString();
        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            edDatatype.Text = "";
            edCustomValue.Text = "";
            edCargowiseValue.Text = "";
            cw_ID = Guid.Empty;
            edDatatype.Focus();
        }

        private void edDatatype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true;
            }
        }
    }
}
