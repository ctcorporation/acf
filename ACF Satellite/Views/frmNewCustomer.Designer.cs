﻿namespace ACF_Satellite
{
    partial class frmNewCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewCustomer));
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edCustomerName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edCustCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gbTrialPeriod = new System.Windows.Forms.GroupBox();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label31 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.cbTrial = new System.Windows.Forms.CheckBox();
            this.cbFtpClient = new System.Windows.Forms.CheckBox();
            this.cbHold = new System.Windows.Forms.CheckBox();
            this.cbActive = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.edRootPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edShortName = new System.Windows.Forms.TextBox();
            this.gbAccountOptions = new System.Windows.Forms.GroupBox();
            this.gbTrialPeriod.SuspendLayout();
            this.gbAccountOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(357, 243);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Customer Name";
            // 
            // edCustomerName
            // 
            this.edCustomerName.Location = new System.Drawing.Point(122, 26);
            this.edCustomerName.Name = "edCustomerName";
            this.edCustomerName.Size = new System.Drawing.Size(296, 20);
            this.edCustomerName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Code or Reference";
            // 
            // edCustCode
            // 
            this.edCustCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCustCode.Location = new System.Drawing.Point(123, 61);
            this.edCustCode.Name = "edCustCode";
            this.edCustCode.Size = new System.Drawing.Size(80, 20);
            this.edCustCode.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Customer Short Name";
            // 
            // gbTrialPeriod
            // 
            this.gbTrialPeriod.Controls.Add(this.dtTo);
            this.gbTrialPeriod.Controls.Add(this.label31);
            this.gbTrialPeriod.Controls.Add(this.dtFrom);
            this.gbTrialPeriod.Controls.Add(this.label30);
            this.gbTrialPeriod.Location = new System.Drawing.Point(12, 194);
            this.gbTrialPeriod.Name = "gbTrialPeriod";
            this.gbTrialPeriod.Size = new System.Drawing.Size(295, 50);
            this.gbTrialPeriod.TabIndex = 46;
            this.gbTrialPeriod.TabStop = false;
            this.gbTrialPeriod.Text = "Trial Period";
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(177, 17);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(101, 20);
            this.dtTo.TabIndex = 3;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(145, 21);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(21, 15);
            this.label31.TabIndex = 2;
            this.label31.Text = "To";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtFrom.Location = new System.Drawing.Point(44, 17);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(98, 20);
            this.dtFrom.TabIndex = 1;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(36, 15);
            this.label30.TabIndex = 0;
            this.label30.Text = "From";
            // 
            // cbTrial
            // 
            this.cbTrial.AutoSize = true;
            this.cbTrial.Location = new System.Drawing.Point(329, 22);
            this.cbTrial.Name = "cbTrial";
            this.cbTrial.Size = new System.Drawing.Size(85, 19);
            this.cbTrial.TabIndex = 6;
            this.cbTrial.Text = "Trial Mode";
            this.cbTrial.UseVisualStyleBackColor = true;
            // 
            // cbFtpClient
            // 
            this.cbFtpClient.AutoSize = true;
            this.cbFtpClient.Location = new System.Drawing.Point(203, 22);
            this.cbFtpClient.Margin = new System.Windows.Forms.Padding(2);
            this.cbFtpClient.Name = "cbFtpClient";
            this.cbFtpClient.Size = new System.Drawing.Size(123, 19);
            this.cbFtpClient.TabIndex = 5;
            this.cbFtpClient.Text = "FTP Client Access";
            this.cbFtpClient.UseVisualStyleBackColor = true;
            // 
            // cbHold
            // 
            this.cbHold.AutoSize = true;
            this.cbHold.Location = new System.Drawing.Point(83, 22);
            this.cbHold.Margin = new System.Windows.Forms.Padding(2);
            this.cbHold.Name = "cbHold";
            this.cbHold.Size = new System.Drawing.Size(113, 19);
            this.cbHold.TabIndex = 4;
            this.cbHold.Text = "Account on hold";
            this.cbHold.UseVisualStyleBackColor = true;
            // 
            // cbActive
            // 
            this.cbActive.AutoSize = true;
            this.cbActive.Location = new System.Drawing.Point(12, 22);
            this.cbActive.Margin = new System.Windows.Forms.Padding(2);
            this.cbActive.Name = "cbActive";
            this.cbActive.Size = new System.Drawing.Size(69, 19);
            this.cbActive.TabIndex = 3;
            this.cbActive.Text = "Is Active";
            this.cbActive.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(357, 208);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // edRootPath
            // 
            this.edRootPath.Location = new System.Drawing.Point(122, 98);
            this.edRootPath.Margin = new System.Windows.Forms.Padding(2);
            this.edRootPath.Name = "edRootPath";
            this.edRootPath.ReadOnly = true;
            this.edRootPath.Size = new System.Drawing.Size(296, 20);
            this.edRootPath.TabIndex = 40;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 101);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 42;
            this.label4.Text = "Root Path";
            // 
            // edShortName
            // 
            this.edShortName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edShortName.Location = new System.Drawing.Point(338, 61);
            this.edShortName.Name = "edShortName";
            this.edShortName.Size = new System.Drawing.Size(80, 20);
            this.edShortName.TabIndex = 2;
            // 
            // gbAccountOptions
            // 
            this.gbAccountOptions.Controls.Add(this.cbActive);
            this.gbAccountOptions.Controls.Add(this.cbHold);
            this.gbAccountOptions.Controls.Add(this.cbFtpClient);
            this.gbAccountOptions.Controls.Add(this.cbTrial);
            this.gbAccountOptions.Location = new System.Drawing.Point(12, 135);
            this.gbAccountOptions.Name = "gbAccountOptions";
            this.gbAccountOptions.Size = new System.Drawing.Size(420, 53);
            this.gbAccountOptions.TabIndex = 48;
            this.gbAccountOptions.TabStop = false;
            this.gbAccountOptions.Text = "Account Options";
            // 
            // frmNewCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 278);
            this.Controls.Add(this.gbAccountOptions);
            this.Controls.Add(this.edShortName);
            this.Controls.Add(this.gbTrialPeriod);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.edRootPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edCustCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edCustomerName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmNewCustomer";
            this.Text = "New Satellite Installation";
            this.Load += new System.EventHandler(this.frmNewCustomer_Load);
            this.gbTrialPeriod.ResumeLayout(false);
            this.gbTrialPeriod.PerformLayout();
            this.gbAccountOptions.ResumeLayout(false);
            this.gbAccountOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edCustomerName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edCustCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbTrialPeriod;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox cbTrial;
        private System.Windows.Forms.CheckBox cbFtpClient;
        private System.Windows.Forms.CheckBox cbHold;
        private System.Windows.Forms.CheckBox cbActive;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox edRootPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edShortName;
        private System.Windows.Forms.GroupBox gbAccountOptions;
    }
}