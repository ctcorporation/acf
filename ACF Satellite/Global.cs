﻿using CTCLogging;
using SatelliteData.Models;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace ACF_Satellite
{
    static class Global
    {

        internal static void LogError(string profile, string details, string process, DateTime time, string error, string path)
        {
            var appLogger = typeof(AppLogger);
            var strArgs = new Type[] { typeof(string), typeof(string), typeof(string), typeof(string), typeof(DateTime), typeof(string), typeof(string) };
            ConstructorInfo constructorAppLogger = appLogger.GetConstructor(strArgs);
            var objLog = constructorAppLogger.Invoke(new object[] { Globals.AppLogPath, profile, details, process, time, error, path });
            var logMethod = appLogger.GetMethod("AddLog");
            var addLog = logMethod.Invoke(objLog, new object[] { });
        }
        public static Guid GetCSVFileType(string fileName)
        {
            Guid result = Guid.Empty;
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var profList = (from f in db.FileDescriptions
                                join p in db.Profiles on f.PC_P equals p.P_ID
                                select new { p.P_ID, p.P_DESCRIPTION, f.PC_FirstFieldName, f.PC_Delimiter, f.PC_HeaderStart, f.PC_FieldCount, f.PC_LastFieldName, f.PC_Id }).ToList();
                if (profList.Count > 0)
                {
                    foreach (var p in profList)
                    {
                        try
                        {
                            string headerText = string.Empty;
                            StreamReader sr = new StreamReader(fileName);

                            for (int i = 0; i <= p.PC_HeaderStart; i++)
                            {
                                sr.ReadLine();
                            }


                            headerText = sr.ReadLine();
                            if (!string.IsNullOrEmpty(headerText))
                            {
                                string[] headers = headerText.Split(Convert.ToChar(p.PC_Delimiter));
                                if (headers.Length == p.PC_FieldCount)
                                {
                                    if (!string.IsNullOrEmpty(p.PC_FirstFieldName))
                                    {
                                        if (p.PC_FirstFieldName == headers[0])
                                        {
                                            if (!string.IsNullOrEmpty(p.PC_LastFieldName))
                                            {
                                                if (p.PC_LastFieldName == headers[headers.Length - 1])
                                                {
                                                    return p.PC_Id;
                                                }

                                            }
                                            else
                                            {
                                                return p.PC_Id;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string strEx = ex.GetType().Name;

                            MethodBase m = MethodBase.GetCurrentMethod();
                            string v = string.Format("{0}.{1}", m.ReflectedType.Name, m.Name);
                            using (AppLogger log = new AppLogger(Globals.AppLogPath, p.P_DESCRIPTION, "Error " + strEx + ": " + ex.Message, v, DateTime.Now, string.Empty, string.Empty))
                            {
                                log.AddLog();
                            }
                        }
                    }

                }

            }
            return result;

        }

        public static void CreateProfPath(string path)
        {
            try
            {
                //string profilePath = Path.Combine(path, profile);
                Directory.CreateDirectory(path);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public static void CreateRootPath(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    string rootPath = Path.Combine(path);
                    Directory.CreateDirectory(rootPath);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Failed"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Custom"));
                }
                else
                {
                    Directory.CreateDirectory(path);
                    string rootPath = Path.Combine(path);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Failed"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Custom"));
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public static string MoveFile(string fileToMove, string folder)
        {
            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(fileToMove);
            string ext = Path.GetExtension(fileToMove);
            string newfile = Path.Combine(folder, Path.GetFileName(fileToMove));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }

            try
            {
                File.Move(fileToMove, Path.Combine(folder, newfile));
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                newfile = "Warning: " + strEx + " exception found while moving " + filename;
            }
            return newfile;
        }

        public static Profile GetProfile(Guid profID)
        {
            using (SatelliteEntity db = new SatelliteEntity(Globals.ConnString.ConnString))
            {
                var profile = (from p in db.Profiles
                               where p.P_ID == profID
                               select p).FirstOrDefault();
                return profile;

            }
        }
    }
}

