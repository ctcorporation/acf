﻿using System;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

namespace ACF_Satellite
{
    [System.Runtime.InteropServices.Guid("B2BDBBE5-A27E-4B19-9C39-BDDCFE037E6A")]
    public static class CsvHelper
    {

        public static string ReplaceFirstOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.IndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        public static string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int Place = Source.LastIndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            try
            {
                StreamReader sr = new StreamReader(strFilePath);
                string[] headers = sr.ReadLine().Split(',');
                DataTable dt = new DataTable();
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {

                    string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    if (headers.Length == rows.Length)
                    {

                        DataRow dr = dt.NewRow();
                        for (int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i];
                        }
                        bool emptyCol = true;
                        for (int i = 0; i < dr.ItemArray.Length; i++)
                        {
                            //Replace Empty Strings "" with string.empty

                            // Remove the first and last " from a string 

                            if (string.IsNullOrEmpty(dr[i].ToString()))
                            {
                               // emptyCol = true;
                                dr[i] = string.Empty;
                            }
                            else
                            {
                                if (dr[i].ToString() == "\"\"")
                                {
                                    dr[i] = string.Empty;
                                }
                                // Replace "" in a string with a single ' 
                                if (dr[i].ToString().Contains("\"\""))
                                {
                                    dr[i] = dr[i].ToString().Replace("\"\"", "'");
                                }
                                if (dr[i].ToString().Contains("'"))
                                {
                                    dr[i].ToString().Replace("'", @"\'");

                                }
                                emptyCol = false;
                                if (dr[i].ToString().Length > 0)
                                {
                                    char firstChar = dr[i].ToString()[0];
                                    char lastChar = dr[i].ToString()[dr[i].ToString().Length - 1];

                                    if (firstChar == Convert.ToChar("\'"))
                                    {
                                        string newString = dr[i].ToString();
                                        newString = ReplaceFirstOccurrence(newString, "\"", string.Empty);
                                        dr[i] = newString;
                                    }
                                    if (lastChar == Convert.ToChar("\'"))
                                    {
                                        string newString = dr[i].ToString();
                                        newString = ReplaceLastOccurrence(newString, "\"", string.Empty);
                                        dr[i] = newString;
                                    }
                                }

                            }


                        }
                        if (!emptyCol)
                        {
                            dt.Rows.Add(dr);
                        }

                    }
                }
                sr.Close();

                return dt;
            }
            catch (IOException ex)
            {
                return null;
            }

        }
    }
}
