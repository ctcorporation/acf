﻿using System.Linq;

namespace ACF_Satellite.Resources
{
    public static class TrimStrings
    {
        public static TSelf TrimStringProps<TSelf>(this TSelf input)
        {
            var stringProps = input.GetType().GetProperties().Where(p => p.PropertyType == typeof(string));
            foreach (var stringProp in stringProps)
            {
                string currVal = (string)stringProp.GetValue(input, null);
                if (currVal != null)
                {
                    stringProp.SetValue(input, currVal.Trim(), null);
                }
            }
            return input;
        }
    }
}
