﻿//using NodeResources;
using NodeResources;
using SatelliteData.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace ACF_Satellite
{
    public static class Globals
    {
        private static IMailServerSettings _mailServerSettings
        { get; set; }
        private static NodeData.IConnectionManager _ctcNodeConnString { get; set; }

        private static string _appPath { get; set; }
        private static string _tempLocation { get; set; }
        private static string _archiveLocation { get; set; }
        private static string _primaryLocation { get; set; }
        private static string _testTempLocation { get; set; }
        private static string _testArchiveLocation { get; set; }
        private static string _testPrimaryLocation { get; set; }
        private static string _testPickupLocation { get; set; }
        private static string _pickupLocation { get; set; }
        private static string _outputLocation { get; set; }
        private static string _libLocation { get; set; }
        private static int _pollTime { get; set; }
        private static int _refeshTime { get; set; }
        private static string _customFilesPath { get; set; }
        private static string _custCode { get; set; }
        private static Customer _cust { get; set; }
        private static string _failPath { get; set; }


        private static NodeData.IConnectionManager _connString { get; set; }
        private static string _appConfig
        {
            get; set;
        }

        public static IMailServerSettings MailServerSettings
        {
            get { return _mailServerSettings; }
            set { _mailServerSettings = value; }
        }

        public static string Support = "csr@ctcorp.com.au";

        public static string TempLocation
        {
            get { return _tempLocation; }
            set { _tempLocation = value; }
        }
        public static string ArchiveLocation
        {
            get { return _archiveLocation; }
            set { _archiveLocation = value; }
        }
        public static string PrimaryLocation
        {
            get { return _primaryLocation; }
            set { _primaryLocation = value; }
        }
        public static string TestPickupLocation
        {
            get { return _testPickupLocation; }
            set { _testPickupLocation = value; }
        }
        public static string PickupLocation
        {
            get { return _pickupLocation; }
            set { _pickupLocation = value; }
        }
        public static string TestTempLocation
        {
            get { return _testTempLocation; }
            set { _testTempLocation = value; }
        }
        public static string TestArchiveLocation
        {
            get { return _testArchiveLocation; }
            set { _testArchiveLocation = value; }
        }
        public static string TestPrimaryLocation
        {
            get { return _testPrimaryLocation; }
            set { _testPrimaryLocation = value; }
        }
        public static string AppPath
        {
            get { return _appPath; }
            set { _appPath = value; }
        }

        public static int PollTime
        {
            get { return _pollTime; }
            set { _pollTime = value; }
        }

        public static int RefreshTime
        {
            get { return _refeshTime; }
            set { _refeshTime = value; }
        }

        public static string AppConfig
        {
            get { return _appConfig; }
            set { _appConfig = value; }
        }
        public static NodeData.IConnectionManager ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }

        public static NodeData.IConnectionManager CTCNodeConnString
        {
            get { return _ctcNodeConnString; }
            set { _ctcNodeConnString = value; }
        }
        public static string AlertsTo { get; internal set; }
        public static string AppLogPath
        {
            get { return _appPath; }
            set { _appPath = value; }
        }
        public static string CustomFilesPath
        {
            get { return _customFilesPath; }
            set { _customFilesPath = value; }
        }

        public static string CustCode
        {
            get { return _custCode; }
            set { _custCode = value; }
        }

        public static string FailPath
        {
            get { return _failPath; }
            set { _failPath = value; }
        }

        public static Customer Customer
        {
            get { return _cust; }
            set { _cust = value; }

        }

        public static string LibPath
        {
            get { return _libLocation; }
            set { _libLocation = value; }
        }

        public static string OutputLocation
        {
            get
            { return _outputLocation; }
            set { _outputLocation = value; }
        }
        public static Customer GetCustomer(string custCode, string connString)
        {
            try
            {
                using (SatelliteEntity db = new SatelliteEntity(connString))
                {
                    Customer cust = (from c in db.Customers
                                     where c.C_CODE == custCode
                                     select new Customer { Code = c.C_CODE, CustomerName = c.C_NAME, Id = c.C_ID, ShortName = c.C_SHORTNAME.Trim() }).FirstOrDefault();
                    if (cust != null)
                    {
                        _cust = cust;
                        return cust;
                    }
                    else
                    {
                        MessageBox.Show("No Customer details record. Please enter Customer Location and Customer Code first.");
                        frmNewCustomer NewCustomer = new frmNewCustomer();
                        NewCustomer.CustCode = custCode;
                        DialogResult dr = new DialogResult();
                        dr = NewCustomer.ShowDialog();

                    }

                }
            }
            catch (InvalidOperationException ex)
            {
                string strEx = ex.Message;
                MessageBox.Show("Error: " + ex.Message, "Error Accessing Database", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;

        }
    }
}
